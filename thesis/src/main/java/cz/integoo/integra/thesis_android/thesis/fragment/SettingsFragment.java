package cz.integoo.integra.thesis_android.thesis.fragment;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.SettingsActivity;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import cz.integoo.integra.thesis_android.thesis.service.LogOutService;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.ToastMessage;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.RangeTimePickerDialog;

/**
 * Created by horkavlna on 25/03/14.
 */
public class SettingsFragment extends PreferenceFragment /*implements SharedPreferences.OnSharedPreferenceChangeListener */{

    private static final String TAG = SettingsFragment.class.getName();
	private static final String INTENT_CONNECTION_RECEIVER = "cz.integoo.integra.thesis_android.thesis.connection";

	private PersistentData mPersistentData;
    private SettingsActivity mSettingsActivity;
    private static MenuItem mRefreshMenuItem;

    private SwitchPreference mCheckPushNotification;
    private Preference mButtonServerAddress;
	private Preference mBackgroundTime;
	private Preference mFetchLimit;
    private Preference mDefaultUser;
    private Preference mAutomaticLogOut;
    private Preference mLogOut;
	private boolean mServerAddressChange;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPersistentData = new PersistentData(getActivity());
        mSettingsActivity = (SettingsActivity) getActivity();

        PreferenceManager pm = getPreferenceManager();
        pm.setSharedPreferencesName(PersistentData.SHARED_PREFERENCES_NAME);
        pm.setSharedPreferencesMode(Context.MODE_PRIVATE);

        addPreferencesFromResource(R.xml.fragment_settings);

        mCheckPushNotification = (SwitchPreference) findPreference(PersistentData.KEY_REMOTE_PUSH_NOTIFICATION);
        mCheckPushNotification.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Log.i(TAG, "checkPushNotification:"+newValue);
                return true;
            }
        });

        mButtonServerAddress = findPreference("server_address");
        mButtonServerAddress.setTitle(mPersistentData.getServerAddress());
        mButtonServerAddress.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showServerAddress();
                return true;
            }
        });

        mFetchLimit = findPreference("fetch_limit");
        mFetchLimit.setTitle(getResources().getString(R.string.preference_settings_fetch_limit_title) + ": " + mPersistentData.getFetchLimit());
        mFetchLimit.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showFetchLimit();
                return true;
            }
        });

		mBackgroundTime = findPreference("automatic_fetch_time");
		mBackgroundTime.setTitle(getResources().getString(R.string.preference_settings_automatic_fetch_title) + ": " +
				Support.getDurationTime(mPersistentData.getAutomaticFetchTime()));
		mBackgroundTime.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				showAutomaticFetchTime();
				return true;
			}
		});

        mDefaultUser = findPreference("default_user");
        if ((mPersistentData.getDefaultUser() !=null) && (mPersistentData.getDefaultUser().length() > 0)) {
            mDefaultUser.setTitle(getResources().getString(R.string.preference_settings_default_user) + ": " + mPersistentData.getDefaultUser());
        } else {
            mDefaultUser.setTitle(getResources().getString(R.string.preference_settings_default_user));
        }
        mDefaultUser.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showDefaultUser();
                return true;
            }
        });


        mAutomaticLogOut = findPreference("time_automatic_logout");
        setTitleAutomaticLogOut(getHours(), getMinutes());
        mAutomaticLogOut.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                automaticLogOut();
                return true;
            }
        });

        mLogOut = findPreference("logout");
        mLogOut.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
				prepareLogOutUser();
                return true;
            }
        });
    }

    public void showServerAddress() {
        final View layoutDialog = getActivity().getLayoutInflater().inflate(R.layout.dialog_edit_text, null);
        final EditText serverAddress = (EditText) layoutDialog.findViewById(R.id.editView);
        serverAddress.setText(mPersistentData.getServerAddress());
        serverAddress.selectAll();
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(layoutDialog);
        builder.setTitle(getActivity().getString(R.string.preference_settings_server_address));
		builder.setMessage(getActivity().getString(R.string.preference_settings_server_address_subtitle));
		builder.setPositiveButton(R.string.preference_settings_set, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layoutDialog.getWindowToken(),0);
				if (!serverAddress.getText().toString().equals(mPersistentData.getServerAddress())) {
					logoutUserAndDeleteAllData(serverAddress.getText().toString());
				}
            }
        });
        builder.setNegativeButton(R.string.preference_settings_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layoutDialog.getWindowToken(),0);
                dialog.cancel();
            }
        });
        builder.show();
    }

	public void logoutUserAndDeleteAllData(final String serverAddress) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getActivity().getString(R.string.preference_settings_server_address_delete_data_title));
		builder.setPositiveButton(R.string.preference_settings_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				mPersistentData.putServerAddress(serverAddress);
				mPersistentData.apply();
				mButtonServerAddress.setTitle(serverAddress);
				mServerAddressChange = true;
				logOutUser();
				dialog.cancel();
			}
		});
		builder.setNegativeButton(R.string.preference_settings_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.show();
	}

	public void showAutomaticFetchTime() {
		TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
				long setNewTime = Support.getAutomaticFetchTimeMiliseconds(hourOfDay, minute);
				if (mPersistentData.getAutomaticFetchTime() != setNewTime) {
					Log.i(TAG, "onTimeSet hour:" + hourOfDay + " minute:" + minute);
					AlarmManager alarmManager = (AlarmManager) mSettingsActivity.getSystemService(mSettingsActivity.ALARM_SERVICE);
					PendingIntent pandingIntentAlarmManager = PendingIntent.getBroadcast(mSettingsActivity, PendingIntent.FLAG_NO_CREATE, new Intent(INTENT_CONNECTION_RECEIVER), 0);
					alarmManager.cancel(pandingIntentAlarmManager);

					mBackgroundTime.setTitle(getResources().getString(R.string.preference_settings_automatic_fetch_title) + ": " +
							Support.getDurationTime(setNewTime));
					mPersistentData.putAutomaticFetchTime(setNewTime);
					mPersistentData.apply();
					mSettingsActivity.sendBroadcast(new Intent(Support.INTENT_ALARM_MANAGER_RECEIVER));
				}
			}
		};
		int[] oclock = Support.getAutomaticFetchTime(mPersistentData.getAutomaticFetchTime());
		RangeTimePickerDialog timePickerDialog = new RangeTimePickerDialog(mSettingsActivity, mTimeSetListener, oclock[1], oclock[0], true);
		timePickerDialog.setMin(0, 5);
		timePickerDialog.updateDialogTitle(
				getActivity().getString(R.string.preference_settings_automatic_fetch_title) + " " +
				getActivity().getString(R.string.preference_settings_automatic_fetch_title_clock),
				getActivity().getString(R.string.preference_settings_automatic_fetch_message),
				getActivity().getString(R.string.preference_settings_set),
				getActivity().getString(R.string.preference_settings_cancel));
		timePickerDialog.show();
	}


    public void showFetchLimit() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getString(R.string.preference_settings_fetch_limit_title));
        builder.setMessage(getActivity().getString(R.string.preference_settings_fetch_limit_subtitle));

        int fetchLimitNumber = 20;
        int fetchLimitRange = 5;
        final String[] numberFetch = new String[fetchLimitNumber];
        for(int i=0; i<fetchLimitNumber; i++) {
            numberFetch[i] = String.valueOf((i+1) * fetchLimitRange);
        }
        final NumberPicker numberPicker = new NumberPicker(getActivity());
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(fetchLimitNumber - 1);
        numberPicker.setDisplayedValues(numberFetch);
        numberPicker.setValue(Arrays.asList(numberFetch).indexOf(String.valueOf(mPersistentData.getFetchLimit())));

        builder.setView(numberPicker);
        builder.setPositiveButton(R.string.preference_settings_set, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.i(TAG, "fetch limit:" + numberFetch[numberPicker.getValue()]);
                mPersistentData.putFetchLimit(Integer.valueOf(numberFetch[numberPicker.getValue()]));
                mPersistentData.apply();
                mFetchLimit.setTitle(getResources().getString(R.string.preference_settings_fetch_limit_title) + ": " + numberFetch[numberPicker.getValue()]);
            }
        });
        builder.setNegativeButton(R.string.preference_settings_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void showDefaultUser() {
        final View layoutDialog = getActivity().getLayoutInflater().inflate(R.layout.dialog_edit_text, null);
        final EditText defaultUser = (EditText) layoutDialog.findViewById(R.id.editView);
        defaultUser.setText(mPersistentData.getDefaultUser());
        defaultUser.selectAll();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(layoutDialog);
        builder.setTitle(getActivity().getString(R.string.preference_settings_default_user));
        builder.setPositiveButton(R.string.preference_settings_set, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mPersistentData.putDefaultUser(defaultUser.getText().toString());
                mPersistentData.apply();
                if (defaultUser.getText().toString().length() > 0) {
                    mDefaultUser.setTitle(getResources().getString(R.string.preference_settings_default_user) + ": " + defaultUser.getText().toString());
                } else {
                    mDefaultUser.setTitle(getResources().getString(R.string.preference_settings_default_user));
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layoutDialog.getWindowToken(),0);
            }
        });
        builder.setNegativeButton(R.string.preference_settings_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layoutDialog.getWindowToken(),0);
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void automaticLogOut() {
        Log.i(TAG, "automaticLogOut");
        final TimePickerDialog.Builder builder = new TimePickerDialog.Builder(getActivity());
        builder.setTitle(getActivity().getString(R.string.preference_settings_time_automatic_logout_title));

        Log.i(TAG, "time logout:" + mPersistentData.getTimeLogout());

        final TimePicker timePicker = new TimePicker(getActivity());
        timePicker.setIs24HourView(true);
        timePicker.setCurrentHour(getHours());
        timePicker.setCurrentMinute(getMinutes());

        builder.setView(timePicker);
        builder.setPositiveButton(R.string.preference_settings_set, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                setTitleAutomaticLogOut(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                long hours = TimeUnit.HOURS.toMillis(timePicker.getCurrentHour());
                long minutes = TimeUnit.MINUTES.toMillis(timePicker.getCurrentMinute());
                mPersistentData.putTimeLogout(hours + minutes);
                mPersistentData.apply();
            }
        });
        builder.setNegativeButton(R.string.preference_settings_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.show();
    }

    public void setTitleAutomaticLogOut(Integer hoursTime, Integer minutesTime) {
        Integer hours = hoursTime;
        Integer minutes = minutesTime;
        if (hours == 0 && minutes == 0) {
            mAutomaticLogOut.setTitle(getResources().getString(R.string.preference_settings_time_automatic_logout_subtitle_off));
            return;
        }
        Integer prefixHours = -1;
        Integer prefixMinutes = -1;
        if (hours < 9) {
            prefixHours = 0;
        }
        if (minutes < 9) {
            prefixMinutes = 0;
        }
        StringBuilder title = new StringBuilder();
        if (prefixHours == 0) {
            title.append(prefixHours);
        }
        title.append(hours);
        title.append(":");
        if (prefixMinutes == 0) {
            title.append(prefixMinutes);
        }
        title.append(minutes);
        mAutomaticLogOut.setTitle(getResources().getString(R.string.preference_settings_time_automatic_logout_subtitle) + " " + title.toString());
    }

    public Integer getHours() {
        return (int) ((mPersistentData.getTimeLogout() / (1000*60*60)) % 24);
    }

    public Integer getMinutes() {
        return (int) ((mPersistentData.getTimeLogout() / (1000*60)) % 60);
    }

	private void prepareLogOutUser() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getActivity().getString(R.string.preference_settings_logout_title));
		builder.setPositiveButton(R.string.preference_settings_logout, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				logOutUser();
				dialog.cancel();
			}
		});
		builder.setNegativeButton(R.string.preference_settings_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.show();
	}

    private void logOutUser() {
        Log.i(TAG, "logOutUser");
		mSettingsActivity.denyBackButton();
        User user = mSettingsActivity.getmDatabase().getUser(mPersistentData.getSignedUserName());
        mRefreshMenuItem = mSettingsActivity.getmMenu().findItem(R.id.action_done);
        mRefreshMenuItem.setActionView(R.layout.action_progressbar);
        mRefreshMenuItem.expandActionView();
        mSettingsActivity.getmMenu().findItem(R.id.action_done).setVisible(true);

        Intent intentLogOut = new Intent(getActivity(), LogOutService.class);
        intentLogOut.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerLogOut));
        intentLogOut.putExtra(Support.EXTRA_URL, mPersistentData.getServerAddress() + user.getLocation());
        intentLogOut.putExtra(Support.EXTRA_USERNAME, user.getUsername());
        intentLogOut.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
        intentLogOut.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startService(intentLogOut);
    }

    private Handler handlerLogOut = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handlerLogOut");
			mSettingsActivity.allowBackButton();
			Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
                stopRefreshButton();
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage != null) {
				showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
            } else {
				if (mServerAddressChange) {
					mServerAddressChange = false;
					mSettingsActivity.getmDatabase().removeAllUser();
				}
                mPersistentData.clearPersistentData();
                mPersistentData.apply();
                mSettingsActivity.logOutUserStartActivity();
            }
            stopRefreshButton();
        }
    };

    public void showErrorMessage(String message) {
		new ToastMessage(mSettingsActivity, message, handlerLogOut, R.id.setting);
    }

    public void stopRefreshButton() {
        Log.i(TAG, "stopRefreshButton");
        if (mRefreshMenuItem != null) {
            mRefreshMenuItem.collapseActionView();
            mRefreshMenuItem.setActionView(null);
            mSettingsActivity.getmMenu().findItem(R.id.action_done).setVisible(false);
        }
    }
}
