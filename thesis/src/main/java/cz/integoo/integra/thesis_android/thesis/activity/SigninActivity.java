package cz.integoo.integra.thesis_android.thesis.activity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.base.GeneralActivity;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.persistence.ApplicationDatabase;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import cz.integoo.integra.thesis_android.thesis.service.SigninService;
import cz.integoo.integra.thesis_android.thesis.support.GcmUtils;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.ToastMessage;
import de.greenrobot.event.EventBus;


public class SigninActivity extends GeneralActivity {

    private static final String TAG = SigninActivity.class.getName();
	private static final String INTENT_RESTORE_MENU_ITEM = "restoremenuitem";
	private static final String INTENT_RESTORE_SHOW_ERROR_MESSAGE = "restoreshowerrormessage";

	private static MenuItem refreshMenuItem;
    private static Menu menu;
	private static SigninActivity signinActivity;
    private EditText mEditTextServer;
    private EditText mEditTextPassword;
    private EditText mEditTextUsername;
    private PersistentData mPersistentData;
    private ApplicationDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(SigninActivity.class.toString(), "onCreate");
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signin);
		mPersistentData = new PersistentData(getApplicationContext());
        mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
        callBroadcastReceiverFirstTime();
        if (mPersistentData.getSignedUserName() != null) {
            Log.i(TAG, "user is signed");
            startNewAlertUserIsSigned();
        } else  {
//			setContentView(R.layout.activity_signin);
            Log.i(TAG, "nobody user is not signed");
            mEditTextServer = (EditText) findViewById(R.id.serverName);
            mEditTextUsername = (EditText) findViewById(R.id.loginUsername);
            mEditTextPassword = (EditText) findViewById(R.id.loginPassword);
            mEditTextPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (menu != null) {
                            clickDoneButton();
                            return true;
                        } else {
                            throw new RuntimeException("Action bar is null in " + SigninActivity.class);
                        }
                    }
                    return false;
                }
            });
            setPlaceHolder();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
		signinActivity = this;
    }

	@Override
    protected void onStop() {
        super.onStop();
        ApplicationDatabase.closeManaged();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar, menu);
        menu.findItem(R.id.action_settings).setVisible(false);
		setActionBar();
		if (refreshMenuItem !=null) {
			showAgainProgressBar();
		}
        return super.onCreateOptionsMenu(menu);
    }

	public void setActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setCustomView(R.layout.actionbar_custom);
		TextView textView = (TextView) actionBar.getCustomView().findViewById(R.id.title_actionbar);
		textView.setText(R.string.action_bar_signin);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
		actionBar.setLogo(new ColorDrawable(Color.TRANSPARENT));
		actionBar.setIcon(new ColorDrawable(Color.TRANSPARENT));
		actionBar.setDisplayUseLogoEnabled(false);
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                //REFRESH
                pressDoneButton(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

	public void showAgainProgressBar() {
		Log.i(TAG, "showAgainProgressBar:" + menu);
		MenuItem menuItem = (MenuItem) menu.findItem(R.id.action_done);
		refreshMenuItem = menuItem;
		refreshMenuItem.setActionView(R.layout.action_progressbar);
		refreshMenuItem.expandActionView();
	}

	public void clickDoneButton() {
		Log.i(TAG, "press button done:" + menu.findItem(R.id.action_done));
		MenuItem menuItem = (MenuItem) menu.findItem(R.id.action_done);
		pressDoneButton(menuItem);
	}

	public void callBroadcastReceiverFirstTime() {
        if (mPersistentData.getFirstTimeCallBroadcastReceiver()) {
            mPersistentData.putFirstTimeCallBroadcastReceiver(false);
            mPersistentData.apply();
            sendBroadcast(new Intent(Support.INTENT_ALARM_MANAGER_RECEIVER));
        }
    }

    public void startNewAlertUserIsSigned() {
        Intent intent = new Intent();
        intent.setClassName(getApplicationContext(), MenuAcvitity.class.getName());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void setPlaceHolder() {
        Log.i(TAG, "setPlaceHolder" + mEditTextUsername);
        if ((mPersistentData.getDefaultUser() == null) && (mEditTextUsername.getText().toString().equals(""))) {
            mEditTextUsername.setHint(getString(R.string.placeholder_username));
            mEditTextUsername.setFocusable(true);
            mEditTextUsername.requestFocus();
        }
        else if (mPersistentData.getDefaultUser() != null) {
            mEditTextUsername.setText(mPersistentData.getDefaultUser());
            mEditTextPassword.setFocusable(true);
            mEditTextPassword.requestFocus();
        }
        if (mEditTextPassword.getText().toString().equals("")) {
            mEditTextPassword.setHint(getString(R.string.placeholder_password));
        }
        if (mEditTextServer.getText().toString().equals("")) {
            mEditTextServer.setHint(getString(R.string.placeholder_server));
        }
        mEditTextServer.setText(mPersistentData.getServerAddress());
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    private Handler handlerSignin = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
                stopRefreshButton();
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            Log.w(TAG, "UNCOMMENT FOR RELEASE");
//            if (errorMessage != null) {
//                showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
//            } else {
                mPersistentData.putSignedUserName(mEditTextUsername.getText().toString());  //TODO DELETE - NO USE
                mPersistentData.putServerAddress(mEditTextServer.getText().toString());
                mPersistentData.apply();

                //SAVE TO THE DATABASE
                User previousUser = mDatabase.getUser(mPersistentData.getSignedUserName());
                if (previousUser != null) {
                    if (!previousUser.getUsername().equals(mEditTextUsername.getText().toString())) {
                        mDatabase.removeUser(previousUser);
                    } else {
                        startNewAlertUserIsSigned();
                        stopRefreshButton();
                        return;
                    }
                }
                User user = new User();
                user.setImei(mPersistentData.getRegIdGcm());
                user.setLocation(bundle.getString(Support.EXTRA_RESPONSE_HEADER, null));
                user.setUsername(mEditTextUsername.getText().toString());
                user.setPassword(mEditTextPassword.getText().toString());
                mDatabase.saveUser(user);

                startNewAlertUserIsSigned();
//            }
            stopRefreshButton();
        }
    };

    public void pressDoneButton(MenuItem item) {
		String error = null;

		try {
			Log.i(TAG, "pressDoneButton");
			if(refreshMenuItem == null) {
				error = checkNetworkUsernamePassword();
				refreshMenuItem = item;
				refreshMenuItem.setActionView(R.layout.action_progressbar);
				refreshMenuItem.expandActionView();
//				GCMRegistrar.checkDevice(this);     //RELEASE
//				GCMRegistrar.checkManifest(this);   //RELEASE
				if (error == null) {
					if (Support.checkNetwork(getApplicationContext())) {
						//DOWNLOAD DATA FROM SERVER
//						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);   //RELEASE
//						imm.hideSoftInputFromWindow(item.getActionView().getWindowToken(),0);   //RELEASE
//						EventBus.getDefault().register(this);   //RELEASE
//						GcmUtils.tryRegisterGcm(getApplicationContext());   //RELEASE
                        onEventMainThread("sa"); //TEST
					} else {
						showErrorMessage(getApplicationContext().getString(R.string.alert_messsage_no_internet_connection));
					}
				} else {
					showErrorMessage(error);
				}
			}
		} catch(Exception e) {
			Log.e(TAG, "exception registred GCM:" + e.getMessage());
			showErrorMessage(getApplicationContext().getString(R.string.alert_messsage_gcm));
		}
    }

    public String checkNetworkUsernamePassword() {
        String errorMessage = null;
        if (mEditTextUsername.getText().toString().equals("")) {
            errorMessage = getString(R.string.alert_message_username);
        }
        else if (mEditTextPassword.getText().toString().equals("")) {
            errorMessage = getString(R.string.alert_message_password);
        }
        return errorMessage;
    }

    public void stopRefreshButton() {
        Log.i(TAG, "stopRefreshButton");
        if (refreshMenuItem != null) {
            refreshMenuItem.collapseActionView();
            refreshMenuItem.setActionView(null);
			refreshMenuItem = null;
        }
    }

    public void showErrorMessage(String message) {
		new ToastMessage(signinActivity, message, handlerSignin, R.id.signin);
    }

    public void onEventMainThread(String regId){
        Log.i(TAG, "onEvent");
        EventBus.getDefault().unregister(this);
        mPersistentData.putRegIdGcm(regId);
        mPersistentData.apply();

        Intent intentSignin = new Intent(getApplicationContext(), SigninService.class);
        intentSignin.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerSignin));
        intentSignin.putExtra(Support.EXTRA_URL, mEditTextServer.getText().toString() + Support.URL_AUTHENTICATION);
        //TODO CREATE PARCELABLE AND SEND STRINGENTITY
        intentSignin.putExtra(Support.EXTRA_BODY, createJsonBodyRegistrationId(regId));
        intentSignin.putExtra(Support.EXTRA_CONTENT_TYPE, Support.HEADER_ACCEPT_CONTENT_TYPE);
        intentSignin.putExtra(Support.EXTRA_USERNAME, mEditTextUsername.getText().toString());
        intentSignin.putExtra(Support.EXTRA_PASSWORD, mEditTextPassword.getText().toString());
        intentSignin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startService(intentSignin);
    }

    public String createJsonBodyRegistrationId(String registrationId) {
        JsonObject imeiObject = new JsonObject();
        imeiObject.addProperty(Support.BODY_DEVICE_OS, "android");
        imeiObject.addProperty(Support.BODY_DEVICE_TOKEN, registrationId);
        return imeiObject.toString();
    }
}
