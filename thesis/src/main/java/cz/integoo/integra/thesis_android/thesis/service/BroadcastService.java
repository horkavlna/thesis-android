package cz.integoo.integra.thesis_android.thesis.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;

/**
 * Created by horkavlna on 05/05/14.
 */
public class BroadcastService extends IntentService {
    private static final String TAG = BroadcastService.class.getName();
    private static final String INTENT_CONNECTION_RECEIVER = "cz.integoo.integra.thesis_android.thesis.connection";

    public BroadcastService() {
        super("broadcast service");
    }

    public BroadcastService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
		PersistentData mPersistentData = new PersistentData(getApplicationContext());
		new DownloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mPersistentData.getAutomaticFetchTime());
    }

    @Override
    public boolean stopService(Intent name) {
        Log.i(TAG, " stopService ");
        return super.stopService(name);
    }

    private class DownloadAsyncTask extends AsyncTask<Long, Void, Void> {

        @Override
        protected Void doInBackground(Long... params) {
			Log.i(TAG, " set periodic time on:" + params[0]);
			AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(getApplicationContext().ALARM_SERVICE);
            PendingIntent pandingIntentAlarmManager = PendingIntent.getBroadcast(getApplicationContext(), PendingIntent.FLAG_NO_CREATE, new Intent(INTENT_CONNECTION_RECEIVER), 0);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), params[0], pandingIntentAlarmManager);
            return null;
        }

        protected void onPostExecute(Void result) {

        }
    }
}
