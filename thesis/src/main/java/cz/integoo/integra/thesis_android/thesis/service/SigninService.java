package cz.integoo.integra.thesis_android.thesis.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.util.HashMap;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.support.Support;


/**
 * Created by horkavlna on 27/03/14.
 */
public class SigninService extends IntentService {

    private static final String TAG = SigninService.class.getName();
    private boolean success;

    public SigninService() {
        super("signin service");
    }

    public SigninService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, " onHandleIntent ");
        Bundle bundle = intent.getExtras();
		if (bundle != null) {
            new SigninAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);
        }
    }

    @Override
    public boolean stopService(Intent name) {
        Log.i(TAG, " stopService ");
        return super.stopService(name);
    }

    //ASYNCTASK FOR CONNECTION
    private class SigninAsyncTask extends AsyncTask<Bundle, Void, Boolean> {
        private Messenger mMessanger;
        private SigninHandler mHandler;
		private IntegooRestClient mIntegooRestClient;

		@Override
        protected Boolean doInBackground(Bundle... bundle) {
            Log.i(SigninAsyncTask.class.getName(), " doInBackground ");
            this.mMessanger = (Messenger) bundle[0].get(Support.EXTRA_HANDLER);
            this.mHandler = new SigninHandler();
            return createHttpConnection((String) bundle[0].get(Support.EXTRA_URL), (String) bundle[0].get(Support.EXTRA_BODY),
                    (String) bundle[0].get(Support.EXTRA_CONTENT_TYPE), (String) bundle[0].get(Support.EXTRA_USERNAME), (String) bundle[0].get(Support.EXTRA_PASSWORD));
        }

        protected void onPostExecute(Boolean result) {
            Log.i(SigninAsyncTask.class.getName(), " onPostExecute ");
			if (mHandler != null) {
                try {
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
					if (success == false) {
						mIntegooRestClient.cancel(getApplicationContext());
						bundle.putString(Support.EXTRA_RESPONSE_ERROR, getApplicationContext().getString(R.string.alert_messsage_no_internet_connection));
						message.setData(bundle);
						mMessanger.send(message);
						return;
					} else if (mHandler.onError() == null) {
                        bundle.putString(Support.EXTRA_RESPONSE_HEADER, mHandler.onHeader());
                        bundle.putByteArray(Support.EXTRA_RESPONSE_BODY, mHandler.onResponseBody());
                    }
                    bundle.putString(Support.EXTRA_RESPONSE_ERROR, mHandler.onError());
                    message.setData(bundle);
                    mMessanger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        public boolean createHttpConnection(String url, String body, String contentType, String username, String password) {
			final int CHECK_MILISECONDS = 250;
			int numberIterate = 0;
            success = false;
            mIntegooRestClient = new IntegooRestClient(username, password);
			mIntegooRestClient.post(getApplicationContext(), url, null, body, contentType, this.mHandler);
            while (true) {
                if(success) {
                    break;
                }
				numberIterate = CHECK_MILISECONDS + numberIterate;
				if (numberIterate >= Support.CONNECTION_TIMEOUT) {
					Log.i(SigninAsyncTask.class.getName(), " number iterate expire");
					break;
				}
                try {
                    Thread.sleep(CHECK_MILISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
    }

    //HANDLER HTTP CONNECTION
    private class SigninHandler extends AsyncHttpResponseHandler implements ListenerHttpResponseHandler {

        private Integer mStatusCode;
        private Header[] mHeaders;
        private byte[] mResponseBody;
        private Throwable mError;

        private SigninHandler() {
        }

        @Override
        public void onStart() {
            super.onStart();
            Log.i(TAG, " onStart");
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            super.onSuccess(statusCode, headers, responseBody);
            Log.i(TAG, " onSuccess - statusCode" + statusCode + " headers" +headers.toString());
            mStatusCode = statusCode;
            mHeaders = headers;
            mResponseBody = responseBody;
        }

		@Override
		public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
			super.onFailure(statusCode, headers, responseBody, error);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
		}

		@Override
        public void onFailure(int statusCode, Throwable error, String content) {
            super.onFailure(statusCode, error, content);
            Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
            mStatusCode = statusCode;
            mError = error;
        }

        @Override
        public void onRetry() {
            super.onRetry();
            Log.i(SigninHandler.class.getName(), " onRetry ");
            // Request was retried
        }

        @Override
        public void onProgress(int bytesWritten, int totalSize) {
            super.onProgress(bytesWritten, totalSize);
            Log.d(SigninHandler.class.getName(), " onProgress ");
            // Progress notification
        }

        @Override
        public void onFinish() {
            super.onFinish();
			success = true;
			Log.d(SigninHandler.class.getName(), " onFinish ");
            // Completed the request (either success or failure)
        }

        @Override
        public Integer onStatusCode() {
            Log.i(SigninService.class.toString(), "status code:" + mStatusCode);
            return mStatusCode;
        }

        @Override
        public String onHeader() {
            String userLocation = null;
//            HashMap<String, String> headerMap = new HashMap<String, String>();
            Header[] headers = mHeaders;
            for (Header header : headers) {
                Log.i(SigninService.class.toString(), "header value:"+ header.getName() +" value:" + header.getValue());
                if (header.getName().equals(Support.HEADER_LOCATION)) {
                    userLocation = header.getValue();
                }
//                headerMap.put(header.getName(), header.getValue());
            }
            return userLocation;
        }

        @Override
        public byte[] onResponseBody() {
            Log.i(SigninService.class.toString(), "body:"+ new String(mResponseBody));
            return mResponseBody;
        }

        @Override
        public String onError() {
            Log.i(SigninService.class.toString(), "errorMessage:"+ mError);
            if (mError == null) {
                return null;
            }
            return mError.toString();
        }
    }
}
