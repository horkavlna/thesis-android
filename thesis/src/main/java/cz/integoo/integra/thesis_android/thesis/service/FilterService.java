package cz.integoo.integra.thesis_android.thesis.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.util.HashMap;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;

/**
 * Created by horkavlna on 30/03/14.
 */
public class FilterService extends IntentService {

    private static final String TAG = FilterService.class.getName();
	private boolean success;

	public FilterService() {
        super("filter service");
    }

    public FilterService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, " onHandleIntent ");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            new FilterAsyncTask(bundle).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        }
    }

    @Override
    public boolean stopService(Intent name) {
        Log.d(TAG, " stopService ");
        return super.stopService(name);
    }

    //ASYNCTASK FOR CONNECTION
    private class FilterAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private Messenger mMessanger;
        private FilterHandler mHandler;
        private Bundle mBundle;
		private IntegooRestClient mIntegooRestClient;

		private FilterAsyncTask() {
		}

		private FilterAsyncTask(Bundle bundle) {
            this.mBundle = bundle;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.i(TAG, " doInBackground url:" + mBundle.get(Support.EXTRA_URL));
            this.mMessanger = (Messenger) mBundle.get(Support.EXTRA_HANDLER);
            this.mHandler = new FilterHandler(this);
            RequestParams requestParams = new RequestParams();
            if (mBundle.containsKey(Support.EXTRA_REQUEST_PARAM)) {
                UrlRequestParam urlRequestParam = mBundle.getParcelable(Support.EXTRA_REQUEST_PARAM);
                requestParams.add(Support.URL_REQUEST_PARAM_FIELDS, urlRequestParam.getFields());
            }
			return createHttpConnection((String) mBundle.get(Support.EXTRA_METHOD), (String) mBundle.get(Support.EXTRA_URL), requestParams,
                    (HashMap) mBundle.get(Support.EXTRA_HEADER), (String) mBundle.get(Support.EXTRA_USERNAME), (String) mBundle.get(Support.EXTRA_PASSWORD));
        }

        protected void onPostExecute(Boolean result) {
            Log.i(TAG, " onPostExecute");
			if (mHandler != null) {
				sendBackHandler();
			}
        }


        public void sendBackHandler() {
            try {
                Message message = Message.obtain();
                Bundle bundle = new Bundle();
				if (success == false) {
					mIntegooRestClient.cancel(getApplicationContext());
					bundle.putString(Support.EXTRA_RESPONSE_ERROR, getApplicationContext().getString(R.string.alert_messsage_no_internet_connection));
					message.setData(bundle);
					mMessanger.send(message);
					return;
				} else if (mHandler.onError() == null) {
                    bundle.putString(Support.EXTRA_RESPONSE_HEADER, mHandler.onHeader());
                    bundle.putByteArray(Support.EXTRA_RESPONSE_BODY, mHandler.onResponseBody());     //TODO CREATE PARCELABLE AND CHANGE LIST TO SET
                }
                bundle.putString(Support.EXTRA_RESPONSE_ERROR, mHandler.onError());
                message.setData(bundle);
                mMessanger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public boolean createHttpConnection(String method, String url, RequestParams requestParams, HashMap<String, String> header, String username, String password) {
			final int CHECK_MILISECONDS = 250;
			int numberIterate = 0;
			success = false;
			mIntegooRestClient = new IntegooRestClient(username, password);
            if (method.equals(Support.GET)) {
				mIntegooRestClient.get(getApplicationContext(), url, requestParams, header, this.mHandler);
            }
            if (method.equals(Support.POST)) {
				mIntegooRestClient.post(getApplicationContext(), url, header, this.mHandler);
            }
			while (true) {
				if(success) {
					break;
				}
				numberIterate = CHECK_MILISECONDS + numberIterate;
				if (numberIterate >= Support.CONNECTION_TIMEOUT) {
					Log.i(FilterAsyncTask.class.getName(), " number iterate expire");
					break;
				}
				try {
					Thread.sleep(CHECK_MILISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
            return true;
        }
	}

    //HANDLER HTTP CONNECTION
    private class FilterHandler extends AsyncHttpResponseHandler implements ListenerHttpResponseHandler {

        private Integer mStatusCode;
        private Header[] mHeaders;
        private byte[] mResponseBody;
        private Throwable mError;
		private FilterAsyncTask mFilterAsyncTask;

        private FilterHandler(FilterAsyncTask filterAsyncTask) {
			this.mFilterAsyncTask = filterAsyncTask;
        }

        @Override
        public void onStart() {
            super.onStart();
            Log.i(FilterHandler.class.getName(), " onStart ");
            // Initiated the request
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            super.onSuccess(statusCode, headers, responseBody);
            Log.i(TAG, " onSuccess - statusCode" + statusCode + " headers" +headers.toString());
            mStatusCode = statusCode;
            mHeaders = headers;
            mResponseBody = responseBody;
		}

		//I think this is bug in library version 1.4.4 one is call onFailure method and one depreceated onFailutre
		@Override
		public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
			super.onFailure(statusCode, headers, responseBody, error);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
		}

        @Override
        public void onFailure(int statusCode, Throwable error, String content) {
            super.onFailure(statusCode, error, content);
            Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
            mStatusCode = statusCode;
            mError = error;
        }

        @Override
        public void onRetry() {
            super.onRetry();
            Log.i(TAG, " onRetry ");
            // Request was retried
        }

        @Override
        public void onProgress(int bytesWritten, int totalSize) {
            super.onProgress(bytesWritten, totalSize);
            Log.i(TAG, " onProgress ");
            // Progress notification
        }

		// Completed the request (either success or failure)
		@Override
        public void onFinish() {
            super.onFinish();
            Log.i(TAG, " onFinish ");
			success = true;
		}

        @Override
        public Integer onStatusCode() {
            Log.i(TAG, "status code:" + mStatusCode);
            return mStatusCode;
        }

        @Override
        public String onHeader() {
            String count = null;
            Header[] headers = mHeaders;
            for (Header header : headers) {
                Log.i(TAG, "header response key:"+ header.getName() +" value:" + header.getValue());
                if (header.getName().equals(Support.HEADER_X_COUNT)) {
                    count = header.getValue();
                }
            }
            return count;
        }

        @Override
        public byte[] onResponseBody() {
            Log.i(TAG, "body:"+ new String(mResponseBody));
            return mResponseBody;
        }

        @Override
        public String onError() {
            Log.i(TAG, "errorMessage:"+ mError);
            if (mError == null) {
                return null;
            }
            return mError.toString();
        }
    }
}
