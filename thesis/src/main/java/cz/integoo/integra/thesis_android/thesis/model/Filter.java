package cz.integoo.integra.thesis_android.thesis.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by horkavlna on 29/03/14.
 */

@DatabaseTable
public class Filter {

    public static final String COLUMN_ID_FILTER = "_id";
	public static final String COLUMN_ID_INTEGOO = "id";
	public static final String COLUMN_NAME = "name";
    public static final String COLUMN_USER = "user";

	@DatabaseField(generatedId = true, columnName = COLUMN_ID_FILTER)
	private long idfilter;
    @SerializedName(COLUMN_ID_INTEGOO)
    @DatabaseField(columnName = COLUMN_ID_INTEGOO)
    private long id;
    @SerializedName(COLUMN_NAME)
    @DatabaseField(columnName = COLUMN_NAME)
    private String name;
    @ForeignCollectionField(eager = false, orderColumnName = Event.COLUMN_CREATED)
    private Collection<Event> events;
    @DatabaseField(canBeNull = false, foreign = true, columnName = COLUMN_USER)
    private User user;

	public long getIdfilter() {
		return idfilter;
	}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Event> getEvents() {
        return events;
    }

    public void setEvents(Collection<Event> events) {
        this.events = events;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Filter)) {
            return false;
        }
        Filter c = (Filter) o;
        return c.getId() == getId();
    }

	@Override
	public String toString() {
		return "Filter{" + "idfilter=" + idfilter + ", id=" + id + ", name='" + name + '\'' + ", events=" + events + ", user=" + user + '}';
	}
}
