package cz.integoo.integra.thesis_android.thesis.support;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;
import cz.integoo.integra.thesis_android.thesis.model.Event;
import cz.integoo.integra.thesis_android.thesis.model.Filter;
import cz.integoo.integra.thesis_android.thesis.model.ImageIcon;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBarUtils;

/**
 * Created by horkavlna on 26/03/14.
 */
public class Support {

    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String DELETE = "DELETE";
    public static final String PUT = "PUT";

    public static final String URL_ALERTS = "/v1/alerts";
    public static final String URL_EVENTS = "/v1/events";
    public static final String URL_ALERT_CONFIRM = "/api/new/alerts/";
    public static final String URL_AUTHENTICATION = "/v1/installations";
    public static final String URL_EVENTS_IMAGE = "/api/events/";
    public static final String URL_IMAGE_ICON = "/v1/event-types";
    public static final String URL_IMAGE = "/img";
    public static final String URL_FILTER = "/v1/event-filters";

    public static final String URL_REQUEST_PARAM_FIELDS = "fields";
    public static final String URL_REQUEST_PARAM_STATUS = "status";

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_ACCEPT = "Accept";
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_LOCATION = "Location";
    public static final String HEADER_X_CREATED = "X-From";
    public static final String HEADER_X_TO = "X-To";
    public static final String HEADER_X_LIMIT = "X-Limit";
    public static final String HEADER_X_OFFSET = "X-Offset";
    public static final String HEADER_X_FILTER = "X-Filter";
    public static final String HEADER_X_COUNT = "X-Total";

    public static final String BODY_DEVICE_OS = "device_os";
    public static final String BODY_DEVICE_TOKEN = "device_token";

    public static final String HEADER_ACCEPT_CONTENT_TYPE = "application/json";
    public static final Integer CONNECTION_TIMEOUT = 30000;
	public static final Integer OPAQUE_COLOR = 153;

	public static final String EXTRA_HANDLER ="cz.integoo.integra.service.signinservice.handler";
    public static final String EXTRA_BODY="cz.integoo.integra.service.signinservice.body";
    public static final String EXTRA_CONTENT_TYPE="cz.integoo.integra.service.signinservice.contenttype";
    public static final String EXTRA_IS_ALERT ="cz.integoo.integra.service.signinservice.is.alert";

    public static final String EXTRA_METHOD="cz.integoo.integra.service.signinservice.method";
    public static final String EXTRA_URL="cz.integoo.integra.service.signinservice.url";
    public static final String EXTRA_REQUEST_PARAM="cz.integoo.integra.service.signinservice.requestparam";
    public static final String EXTRA_HEADER="cz.integoo.integra.service.signinservice.header";
    public static final String EXTRA_USERNAME="cz.integoo.integra.service.signinservice.username";
    public static final String EXTRA_PASSWORD="cz.integoo.integra.service.signinservice.password";

    public static final String EXTRA_RESPONSE_STATUSCODE="cz.integoo.integra.service.signinservice.response.statuscode";
    public static final String EXTRA_RESPONSE_HEADER="cz.integoo.integra.service.signinservice.response.header";
    public static final String EXTRA_RESPONSE_BODY="cz.integoo.integra.service.signinservice.response.body";
    public static final String EXTRA_RESPONSE_ERROR="cz.integoo.integra.service.signinservice.response.error";

	public static final String INTENT_ALARM_MANAGER_RECEIVER = "cz.integoo.integra.thesis_android.thesis.alarmmanager";

	public static boolean checkNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static String getDurationTimeWithSeconds(Long seconds)  {
        StringBuilder outputTime = new StringBuilder();

        long days = seconds / (60 * 60 * 24);

        seconds -= days * (60 * 60 * 24);
        long hours = seconds / (60 * 60);
        seconds -= hours * (60 * 60);
        long minutes = seconds / 60;
        seconds -= minutes*60;

        if (days > 0) {
            outputTime.append(String.format("%d d ",days));
        }
        if (hours > 0) {
            if (hours < 10) {
                outputTime.append("0");
            }
            outputTime.append(String.format("%d:",hours));
        } else {
            outputTime.append("00:");
        }
        if (minutes > 0) {
            if (minutes < 10) {
                outputTime.append("0");
            }
            outputTime.append(String.format("%d:",minutes));
        } else {
            outputTime.append("00:");
        }
        if (seconds > 0) {
            if (seconds < 10) {
                outputTime.append("0");
            }
            outputTime.append(String.format("%d",seconds));
        } else {
            outputTime.append("00");
        }
        return outputTime.toString();
    }

	public static String getDurationTime(Long miliseconds)  {
		long seconds = miliseconds / 1000;
		StringBuilder outputTime = new StringBuilder();

		long days = seconds / (60 * 60 * 24);

		seconds -= days * (60 * 60 * 24);
		long hours = seconds / (60 * 60);
		seconds -= hours * (60 * 60);
		long minutes = seconds / 60;
		seconds -= minutes*60;

		if (hours > 0) {
			if (hours < 10) {
				outputTime.append("0");
			}
			outputTime.append(String.format("%d:",hours));
		} else {
			outputTime.append("00:");
		}
		if (minutes > 0) {
			if (minutes < 10) {
				outputTime.append("0");
			}
			outputTime.append(String.format("%d",minutes));
		} else {
			outputTime.append("00");
		}
		return outputTime.toString();
	}

	public static int[] getAutomaticFetchTime(Long miliseconds)  {
		long seconds = miliseconds/1000;
		int[] oclock = new int[2];

		long days = seconds / (60 * 60 * 24);

		seconds -= days * (60 * 60 * 24);
		long hours = seconds / (60 * 60);
		seconds -= hours * (60 * 60);
		long minutes = seconds / 60;
		oclock[0] = (int) minutes;
		oclock[1] = (int) hours;

		return oclock;
	}

	public static long getAutomaticFetchTimeMiliseconds(int hours, int minutes)  {
		return (TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS) + TimeUnit.MILLISECONDS.convert(minutes, TimeUnit.MINUTES));
	}


	public static ArrayList<Alert> parseByteArrayAlerts(byte[] byteArray) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AlertStatus.class, new AlertStatusDeserializer());
        Gson gson = gsonBuilder.create();
        Type listType = new TypeToken<List<Alert>>(){}.getType();
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(new String(byteArray)).getAsJsonArray();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static ArrayList<Event> parseByteArrayEvents(byte[] byteArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Event>>(){}.getType();
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(new String(byteArray)).getAsJsonArray();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static ArrayList<ImageIcon> parseByteArrayImageIcon(byte[] byteArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<ImageIcon>>(){}.getType();
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(new String(byteArray)).getAsJsonArray();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static ImageIcon parseByteToImage(byte[] byteArray) {
		Gson gson = new Gson();
		Type listType = new TypeToken<ImageIcon>(){}.getType();
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = jsonParser.parse(new String(byteArray)).getAsJsonObject();
		return gson.fromJson(jsonObject.toString(), listType);
    }

    public static ArrayList<Filter> parseByteToFilter(byte[] byteArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Filter>>(){}.getType();
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(new String(byteArray)).getAsJsonArray();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static Long parseISO8601toDate(String input) {
        long timeIn = 0;
        List<SimpleDateFormat> dataFormatPatterns = new LinkedList<SimpleDateFormat>();
        dataFormatPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz"));
        dataFormatPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz"));
        for (SimpleDateFormat dateFormat : dataFormatPatterns) {
            if (input.endsWith( "Z" )) {
                input = input.substring( 0, input.length() - 1) + "GMT-00:00";
            } else {
                int inset = 6;
                String s0 = input.substring(0, input.length() - inset);
                String s1 = input.substring(input.length() - inset, input.length());
                input = s0 + "GMT" + s1;
            }
            try {
                timeIn = dateFormat.parse(input).getTime();
            } catch (ParseException e) {
                continue;
            }
        }
        return (timeIn/1000);
    }

    public static SimpleDateFormat checkDataFormat(String input) {
        List<SimpleDateFormat> dataFormatPatterns = new LinkedList<SimpleDateFormat>();
        dataFormatPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz"));
        dataFormatPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz"));
        for (SimpleDateFormat dateFormat : dataFormatPatterns) {
            try {
                if (dateFormat.parse(input).getTime() > 0) {
                    return dateFormat;
                }
            } catch (ParseException pe) {
                continue;
            }
        }
        return null;
    }

    public static synchronized HashMap<String, String> createAllAlertsHttpHeader(String fetchLimit, String offset) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(Support.HEADER_X_LIMIT, fetchLimit);
        header.put(Support.HEADER_X_OFFSET, offset);
        return header;
    }

    public static synchronized String createAlarmRequestParam() {
        return new StringBuilder().append(Alert.COLUMN_ID_ALERT_INTEGOO).append(",").append(Alert.COLUMN_NAME).append(",").append(Alert.COLUMN_DEVICE_NAME).
                append(",").append(Alert.COLUMN_PLACE_NAME).append(",").append(Alert.COLUMN_CREATED_AT).append(",").append(Alert.COLUMN_UPDATED_AT).append(",").
                append(Alert.COLUMN_TYPE_COLOR).append(",").append(Alert.COLUMN_INSTRUCTION).append(",").append(Alert.COLUMN_INSTRUCTION_DEF).toString();
    }

    public static synchronized String createEventRequestParam() {
        return new StringBuilder().append(Event.COLUMN_ID_EVENT_INTEGOO).append(",").append(Event.COLUMN_CREATED).append(",").append(Event.COLUMN_USER).
                append(",").append(Event.COLUMN_MESSAGE).append(",").append(Event.COLUMN_DEVICE).append(",").append(Event.COLUMN_PLACE).append(",").toString();
    }

    public static synchronized String createFilterRequestParam() {
        return new StringBuilder().append(Filter.COLUMN_ID_INTEGOO).append(",").append(Filter.COLUMN_NAME).toString();
    }

    public static synchronized String createIconRequestParam() {
        return new StringBuilder().append(ImageIcon.COLUMN_ID_INTEGOO).append(",").append(ImageIcon.COLUMN_IMAGE).toString();
    }

    public static synchronized HashMap<String, String> createAllEventHttpHeader(String fetchLimit, String offset, String filterId) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(Support.HEADER_X_LIMIT, fetchLimit);
        header.put(Support.HEADER_X_OFFSET, offset);
        header.put(Support.HEADER_X_FILTER, filterId);
        return header;
    }
}
