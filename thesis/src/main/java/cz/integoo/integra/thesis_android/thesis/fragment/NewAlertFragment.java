package cz.integoo.integra.thesis_android.thesis.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.MenuAcvitity;
import cz.integoo.integra.thesis_android.thesis.adapter.NewAlertCursorAdapter;
import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.service.MenuService;
import cz.integoo.integra.thesis_android.thesis.support.DetailListViewListener;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.ToastMessage;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by horkavlna on 30/03/14.
 */
public class NewAlertFragment extends ListFragment implements OnRefreshListener {

    private static final String TAG = NewAlertFragment.class.getName();

    private MenuAcvitity mMenuActivity;
    private View mFragmentView;
    private ListView mListview;
    private NewAlertCursorAdapter mNewAlertCursorAdapter;
    private DetailListViewListener mListener;
    private static NewAlertFragment newAlertFragment;
	private TextView mEmptyList;

    @Override
    public void onAttach(Activity activity) {
        Log.i(TAG, "onAttach");
        super.onAttach(activity);
        mListener = (DetailListViewListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mMenuActivity = (MenuAcvitity) getActivity();
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        mFragmentView = inflater.inflate(R.layout.fragment_new_alert_list, container, false);
        if (mMenuActivity.getmPullToRefreshLayoutNewAlert() == null) {
            Log.i(TAG, "setMenuVisibility mPullToRefreshLayout");
            setProgressDialog();
        }
        mListview = (ListView) mFragmentView.findViewById(android.R.id.list);
		mEmptyList = (TextView) mFragmentView.findViewById(R.id.emptyList);
		reloadList();
        return mFragmentView;
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        setInstanceBroadcast(this);
        if (mMenuActivity.getmPersistentData().getSignedUserName() != null) {
            reloadList();
            if (mNewAlertCursorAdapter != null) {
                mNewAlertCursorAdapter.runUpdateTime();
            }
            if (mMenuActivity.getmIdAlertRemotePushNotification() == -1l) {
                showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_bad_remote_push_notification));
            } else if (mMenuActivity.getmIdAlertRemotePushNotification() != 0l) {
                callServiceNewAlert();
				mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(true);
            }
        }
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        setInstanceBroadcast(null);
        if (mNewAlertCursorAdapter != null) {
            mNewAlertCursorAdapter.stopUpdateTime();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    public static NewAlertFragment getInstanceBroadcast() {
        return NewAlertFragment.newAlertFragment;
    }

    public static void setInstanceBroadcast(NewAlertFragment newAlertFragment) {
        NewAlertFragment.newAlertFragment = newAlertFragment;
    }

    public NewAlertCursorAdapter getmNewAlertCursorAdapter() {
        return mNewAlertCursorAdapter;
    }

    public void reloadList() {
        Cursor databaseCursor = mMenuActivity.getmDatabase().getCursorAlert(Alert.class, mMenuActivity.getmPersistentData().getSignedUserName(), AlertStatus.NEW);
        Log.i(TAG,"reloadList newAlert:" + mMenuActivity.getmPersistentData().getSignedUserName() + " getCount: " + databaseCursor.getCount());
        Log.i(TAG,"reloadList existAlert:" + mMenuActivity.getmPersistentData().getSignedUserName() + " getCount: " + mMenuActivity.getmDatabase().getCursorAlert(Alert.class, mMenuActivity.getmPersistentData().getSignedUserName(), AlertStatus.ACCEPTED).getCount());
        if (databaseCursor.getCount() != 0) {
            MatrixCursor extras = new MatrixCursor(new String[] { Alert.COLUMN_ID_ALERT_INTEGOO, Alert.COLUMN_NAME, Alert.COLUMN_DEVICE_NAME, Alert.COLUMN_PLACE_NAME, Alert.COLUMN_CREATED_AT,
                    Alert.COLUMN_UPDATED_AT, Alert.COLUMN_TYPE_COLOR, Alert.COLUMN_INSTRUCTION, Alert.COLUMN_INSTRUCTION_DEF, Alert.COLUMN_STATUS, Alert.COLUMN_IMAGE, Alert.COLUMN_USER});
            extras.addRow(new String[] { "-1", String.format("%s %d %s", mMenuActivity.getString(R.string.alert_next_fetch_first),
                    mMenuActivity.getmPersistentData().getFetchLimit(), mMenuActivity.getString(R.string.alert_next_fetch_second))
                    , null, null, null, null, null, null, null, null, null, null });
            Cursor[] cursors = {databaseCursor, extras};
            Cursor extendedCursor = new MergeCursor(cursors);
			mEmptyList.setVisibility(View.GONE);
            mNewAlertCursorAdapter = new NewAlertCursorAdapter(mMenuActivity,extendedCursor);
            mListview.setAdapter(mNewAlertCursorAdapter);
        } else {
			mEmptyList.setVisibility(View.VISIBLE);
            mListview.setAdapter(null);
        }
    }

    public void setProgressDialog() {
		mMenuActivity.setmPullToRefreshLayoutNewAlert((PullToRefreshLayout) mFragmentView.findViewById(R.id.ptr_layout));
		ActionBarPullToRefresh.from(getActivity())
                // Mark All Children as pullable
                .allChildrenArePullable()
                        // Set a OnRefreshListener
                .listener(this)
                        // Finally commit the setup to our PullToRefreshLayout
                .setup(mMenuActivity.getmPullToRefreshLayoutNewAlert());
    }

    @Override
    public void onRefreshStarted(View view) {
        Log.i(TAG, "onRefreshStarted");
        //DOWNLOAD DATA FROM SERVER
        callServiceNewAlert();
    }

    public void callServiceNewAlert() {
        if (mMenuActivity != null) {
            if (Support.checkNetwork(mMenuActivity)) {
                if (mMenuActivity.getmDatabase().isOpen()) {
                    User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());

                    Intent intentNewAlert = new Intent(mMenuActivity.getApplicationContext(), MenuService.class);
                    intentNewAlert.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerNewAlert));
                    intentNewAlert.putExtra(Support.EXTRA_METHOD, Support.GET);
                    intentNewAlert.putExtra(Support.EXTRA_URL, mMenuActivity.getmPersistentData().getServerAddress() + Support.URL_ALERTS);
                    intentNewAlert.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createAlarmRequestParam(), AlertStatus.NEW));
                    intentNewAlert.putExtra(Support.EXTRA_HEADER, Support.createAllAlertsHttpHeader(mMenuActivity.getmPersistentData().getFetchLimit().toString(), "0"));
                    intentNewAlert.putExtra(Support.EXTRA_IS_ALERT, true);
                    //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                    intentNewAlert.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                    intentNewAlert.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                    intentNewAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mMenuActivity.startService(intentNewAlert);
					mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
                }
            } else {
                showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_no_internet_connection));
            }
        }
    }

    private Handler handlerNewAlert = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
				mMenuActivity.setAllItemPullToRefreshLayoutEnabled(true);
				setProgressDialog();    //beacuse this is bug in progress dialog in first refresh is ok, second is not valid and third is ok
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage != null) {
                showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
            } else {
				mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(false);
				mMenuActivity.setAllItemPullToRefreshLayoutEnabled(true);
				//SAVE TO THE DATABASE
                User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());
				ArrayList<Alert> myModelList = (ArrayList<Alert>) bundle.getSerializable(Support.EXTRA_RESPONSE_BODY);
                for (Alert alert : myModelList) {
                    Log.i(TAG, "download alert:" +alert.toString());
                }
				Log.i(TAG, "handleMessage user:" + user + " array size:" + myModelList.size());
				mMenuActivity.getmDatabase().saveAlerts(myModelList, user, AlertStatus.NEW);
                reloadList();
                Log.i(TAG, "save to database");
                if (mMenuActivity.getmIdAlertRemotePushNotification() != 0l) {
                    Log.i(TAG, "call detail");
                    Alert alert = mMenuActivity.getmDatabase().getAlert(mMenuActivity.getmIdAlertRemotePushNotification());
                    if (alert != null) {
                        mListener.onNewAlertDetailSelected(mMenuActivity.getmIdAlertRemotePushNotification());
                    } else {
                        showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_bad_remote_push_notification));
                    }
                    mMenuActivity.setmIdAlertRemotePushNotification(0l);
                }
            }
        }
    };

    public void showErrorMessage(String message) {
		mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(false);
		mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
		new ToastMessage(mMenuActivity, message, handlerNewAlert, R.id.menu);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        long inndexRow = mNewAlertCursorAdapter.getCursor().getLong(mNewAlertCursorAdapter.getCursor().getColumnIndex(Alert.COLUMN_ID_ALERT_INTEGOO));
        Log.i(TAG, "setOnItemClickListener onListItemClick indexRow:" + inndexRow);

        if (inndexRow == -1) {
            //fetch x limit
            int positionColumn = ((MergeCursor) mNewAlertCursorAdapter.getItem(position - 1)).getColumnIndex(Alert.COLUMN_CREATED_AT);
            String createMinAlert =((MergeCursor) mNewAlertCursorAdapter.getItem(position - 1)).getString(positionColumn);
            Log.i(TAG, "setOnItemClickListener onListItemClick last created id:" + createMinAlert);
            if (createMinAlert != null) {
                if (mMenuActivity != null) {
                    if (Support.checkNetwork(mMenuActivity)) {
                        if (mMenuActivity.getmDatabase().isOpen()) {
                            User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());
                            Intent intentNewAlert = new Intent(mMenuActivity.getApplicationContext(), MenuService.class);
                            intentNewAlert.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerNewAlert));
                            intentNewAlert.putExtra(Support.EXTRA_METHOD, Support.GET);
                            intentNewAlert.putExtra(Support.EXTRA_URL, mMenuActivity.getmPersistentData().getServerAddress() + Support.URL_ALERTS);
                            intentNewAlert.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createAlarmRequestParam(), AlertStatus.NEW));
                            intentNewAlert.putExtra(Support.EXTRA_HEADER,
                                    createNextAlertsHttpHeader(mMenuActivity.getmPersistentData().getFetchLimit().toString(), 0, createMinAlert));
                            intentNewAlert.putExtra(Support.EXTRA_IS_ALERT, true);
                            //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                            intentNewAlert.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                            intentNewAlert.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                            intentNewAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mMenuActivity.startService(intentNewAlert);
							mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(true);
							mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
                        }
                    } else {
                        showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_no_internet_connection));
                    }
                }
            }
        } else {
            //show detail alert
            mListener.onNewAlertDetailSelected(inndexRow);
        }
    }

    private HashMap<String, String> createNextAlertsHttpHeader(String fetchLimit, Integer offset, String from) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(Support.HEADER_X_LIMIT, fetchLimit);
        header.put(Support.HEADER_X_CREATED, from);
        header.put(Support.HEADER_X_OFFSET, offset.toString());
        return header;
    }
}