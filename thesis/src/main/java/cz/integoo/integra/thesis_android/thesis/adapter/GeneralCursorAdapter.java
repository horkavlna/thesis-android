package cz.integoo.integra.thesis_android.thesis.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Date;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import cz.integoo.integra.thesis_android.thesis.support.Support;

/**
 * Created by horkavlna on 08/04/14.
 */
public abstract class GeneralCursorAdapter extends android.support.v4.widget.CursorAdapter {

    private static final String TAG = GeneralCursorAdapter.class.getName();
	private Timer mTimer;
	public Set<ViewHolder> mHolders;

    public GeneralCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    private class CountTimer extends TimerTask {

        @Override
        public void run() {
            new CreatedTask((new Date().getTime()/1000))
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        }
    }

    public static class ViewHolder {
        //NEW ALERT
        RelativeLayout relativeLayout;
        TextView title;
        TextView subtitle;
        TextView created;
        TextView next;
        Long createdTime;

        //FILTER
        TextView titleFilter;
        public RadioButton selectedFilter;

        //EVENT
        ImageView eventIcon;
    }

    private class CreatedTask extends AsyncTask<Void, Void, Void> {
        private Long mActuallTime;

        public CreatedTask(Long actuallTime) {
            this.mActuallTime = actuallTime;
        }
        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        protected void onPostExecute(Void result) {
            if (mHolders.isEmpty()) {
                stopUpdateTime();
            } else {
                for (ViewHolder holder : mHolders) {
					if (holder != null && holder.created != null && holder.createdTime != null) {
						holder.created.setText(Support.getDurationTimeWithSeconds(mActuallTime - holder.createdTime));
					}
                }
            }
        }
    }

    public void runUpdateTime() {
        Log.i(TAG, "runUpdateTime");
		if (mTimer == null) {
			Log.i(TAG, "mTimer == null");
			mTimer = new Timer();
			mTimer.schedule(new CountTimer(), 1000, 1000);
		}

    }

    public void stopUpdateTime() {
        if (mTimer != null) {
            Log.i(TAG, "removeUpdateTime");
            mTimer.cancel();
            mTimer = null;
        }
    }
}
