package cz.integoo.integra.thesis_android.thesis.support;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;

import cz.integoo.integra.thesis_android.thesis.GCMIntentService;
import de.greenrobot.event.EventBus;

/**
 * Created by horkavlna on 29/04/14.
 */
public class GcmUtils {
    private static final String TAG = "GcmUtils";
    private static Handler handlerSignin;
    private static String username;
    private static String password;

    public static boolean tryRegisterGcm(Context context) {
        GCMRegistrar.checkDevice(context);
        GCMRegistrar.checkManifest(context);
        String regId = GCMRegistrar.getRegistrationId(context);

        if (regId.equals("")) {
            GCMRegistrar.register(context, GCMIntentService.SENDER_ID);
            GCMRegistrar.setRegisteredOnServer(context, true);
            Log.i(TAG, "GCM is registered now");
            return true;
        }
        EventBus.getDefault().post(regId);
        Log.i(TAG, "GCM is already registered by id = " + regId);
        return false;
    }

    public static void unregisterGcm(Context context) {
        GCMRegistrar.unregister(context);
        GCMRegistrar.setRegisteredOnServer(context, false);
    }
}
