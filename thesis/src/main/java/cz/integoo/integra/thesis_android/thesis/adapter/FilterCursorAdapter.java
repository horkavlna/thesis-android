package cz.integoo.integra.thesis_android.thesis.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.model.Filter;

/**
 * Created by horkavlna on 08/04/14.
 */
public class FilterCursorAdapter extends android.support.v4.widget.CursorAdapter{

	private static final String TAG = FilterCursorAdapter.class.getName();

	private LayoutInflater mInflater;
    private Set<GeneralCursorAdapter.ViewHolder> mHolders;
    private Long mCheckedFilterId;

    public FilterCursorAdapter(Context context, Cursor cursorUserList, Long checkedFilterId) {
        super(context, cursorUserList, FLAG_REGISTER_CONTENT_OBSERVER);
        //super(context, cursorUserList);
        mInflater = LayoutInflater.from(context);
        mHolders = new HashSet<GeneralCursorAdapter.ViewHolder>();
        this.mCheckedFilterId = checkedFilterId;
		this.mHolders = new HashSet<GeneralCursorAdapter.ViewHolder>();
	}

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        GeneralCursorAdapter.ViewHolder viewHolder;
        View viewLayout = mInflater.inflate(R.layout.adapter_filter, parent, false);
        viewHolder = new GeneralCursorAdapter.ViewHolder();
        viewHolder.titleFilter = (TextView) viewLayout.findViewById(R.id.title_filter);
        viewHolder.selectedFilter = (RadioButton) viewLayout.findViewById(R.id.checkbox_filter);
        viewLayout.setTag(viewHolder);
        return viewLayout;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        GeneralCursorAdapter.ViewHolder holder = (GeneralCursorAdapter.ViewHolder) view.getTag();

        String title = cursor.getString(cursor.getColumnIndexOrThrow(Filter.COLUMN_NAME));
        holder.titleFilter.setText(title);
		if (cursor.getLong(cursor.getColumnIndexOrThrow(Filter.COLUMN_ID_FILTER)) == mCheckedFilterId) {
			holder.selectedFilter.setChecked(true);
        } else {
			holder.selectedFilter.setChecked(false);
		}
        mHolders.add(holder);
    }

	public void setCheckFilter(View view, Cursor cursor){
		GeneralCursorAdapter.ViewHolder holder = (GeneralCursorAdapter.ViewHolder) view.getTag();
		String title = cursor.getString(cursor.getColumnIndexOrThrow(Filter.COLUMN_NAME));
		holder.titleFilter.setText(title);
		holder.selectedFilter.setChecked(true);
		mHolders.add(holder);
	}

}
