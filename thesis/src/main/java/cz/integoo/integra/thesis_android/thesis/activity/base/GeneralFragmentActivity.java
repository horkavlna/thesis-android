package cz.integoo.integra.thesis_android.thesis.activity.base;

import android.support.v4.app.FragmentActivity;
import android.util.Log;

/**
 * Created by horkavlna on 04/05/14.
 */
public class GeneralFragmentActivity extends FragmentActivity {

    private static final String TAG = GeneralFragmentActivity.class.getName();
    private static boolean isAppWentToBackground = true;
    private static boolean isWindowFocused = false;
    private static boolean isBackPressed = false;

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart isAppWentToBackground " + isAppWentToBackground);
        applicationWillEnterForeground();
        super.onStart();
    }

    private void applicationWillEnterForeground() {
        if (isAppWentToBackground && GeneralActivity.isIsAppWentToBackground() && GeneralPreferenceActivity.isIsAppWentToBackground()) {
            isAppWentToBackground = false;
            GeneralActivity.logOutUser(getApplicationContext());
//            Toast.makeText(getApplicationContext(), "App is in foreground", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop ");
        applicationdidenterbackground();
    }

	public void applicationdidenterbackground() {
        if (!isWindowFocused && !GeneralActivity.isIsWindowFocused() && !GeneralPreferenceActivity.isIsWindowFocused()) {
            isAppWentToBackground = true;
            GeneralActivity.setIsAppWentToBackground(true);
            GeneralPreferenceActivity.setIsAppWentToBackground(true);
            GeneralActivity.saveLastTimeInApplication(getApplicationContext());
//            Toast.makeText(getApplicationContext(), "App is Going to Background", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (this instanceof GeneralFragmentActivity) {
        } else {
            isBackPressed = true;
        }
        Log.d(TAG, "onBackPressed " + isBackPressed + "" + this.getLocalClassName());
        super.onBackPressed();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        isWindowFocused = hasFocus;
        Log.i(TAG, "onWindowFocusChanged " + isWindowFocused);
        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public static boolean isIsBackPressed() {
        return isBackPressed;
    }

    public static boolean isIsAppWentToBackground() {
        return isAppWentToBackground;
    }

    public static boolean isIsWindowFocused() {
        return isWindowFocused;
    }

    public static void setIsAppWentToBackground(boolean isAppWentToBackground) {
        GeneralFragmentActivity.isAppWentToBackground = isAppWentToBackground;
    }
}
