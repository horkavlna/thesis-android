package cz.integoo.integra.thesis_android.thesis.support.parceleable;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

import java.lang.reflect.Field;

import cz.integoo.integra.thesis_android.thesis.fragment.SettingsFragment;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;

/**
 * Created by horkavlna on 21/05/14.
 */
public class RangeTimePickerDialog extends TimePickerDialog {

	private int mMinHour = -1;
	private int mMinMinute = -1;

	private int mMaxHour = 25;
	private int mMaxMinute = 25;

	private int mCurrentHour = 0;
	private int mCurrentMinute = 0;

	private Context mContext;
	private PersistentData mPersistentData;
	private SettingsFragment mSettingsFragment;

	public RangeTimePickerDialog(Context context, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
		super(context, callBack, hourOfDay, minute, is24HourView);
		this.mContext = context;
		mPersistentData = new PersistentData(context);
		mCurrentHour = hourOfDay;
		mCurrentMinute = minute;

		try {
			Class<?> superclass = getClass().getSuperclass();
			Field mTimePickerField = superclass.getDeclaredField("mTimePicker");
			mTimePickerField.setAccessible(true);
			TimePicker mTimePicker = (TimePicker) mTimePickerField.get(this);
			mTimePicker.setOnTimeChangedListener(this);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}
	}

	public void setMin(int hour, int minute) {
		mMinHour = hour;
		mMinMinute = minute;
	}

	public void setMax(int hour, int minute) {
		mMaxHour = hour;
		mMaxMinute = minute;
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

		boolean validTime = true;
		if (hourOfDay < mMinHour || (hourOfDay == mMinHour && minute < mMinMinute)){
			validTime = false;
		}

		if (hourOfDay  > mMaxHour || (hourOfDay == mMaxHour && minute > mMaxMinute)){
			validTime = false;
		}

		if (validTime) {
			mCurrentHour = hourOfDay;
			mCurrentMinute = minute;
		}

		updateTime(mCurrentHour, mCurrentMinute);
		//updateDialogTitle(view, mCurrentHour, mCurrentMinute);
	}

	public void updateDialogTitle(String title, String message, String textPositiveButton, String textNegativeButton) {
//		calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
//		calendar.set(Calendar.MINUTE, minute);
//		String title = dateFormat.format(calendar.getTime());
		setTitle(title);
		setMessage(message);
//		setButton(BUTTON_POSITIVE, textPositiveButton, new OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//
//			}
//		});
//		setButton(BUTTON_NEGATIVE, textNegativeButton, new OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//
//			}
//		});
	}
}