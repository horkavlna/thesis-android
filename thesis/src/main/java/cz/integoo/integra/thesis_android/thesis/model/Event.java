package cz.integoo.integra.thesis_android.thesis.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Arrays;

/**
 * Created by horkavlna on 29/03/14.
 */

@DatabaseTable
public class Event {

	public static final String COLUMN_ID_EVENT = "_id";
    public static final String COLUMN_ID_EVENT_INTEGOO = "id";
    public static final String COLUMN_CREATED = "created_at";
    public static final String COLUMN_USER = "user_fullname";
    public static final String COLUMN_MESSAGE = "msg";
    public static final String COLUMN_DEVICE = "device_name";
    public static final String COLUMN_PLACE = "place_name";
    public static final String COLUMN_FILTER = "filter";
    public static final String COLUMN_IMAGE_ICON_FOREIGN = "imgforegin";
    public static final String COLUMN_IMAGE = "image";

	@DatabaseField(generatedId = true, columnName = COLUMN_ID_EVENT)
	private long idEvent;
    @SerializedName(COLUMN_ID_EVENT_INTEGOO)
    @DatabaseField(columnName = COLUMN_ID_EVENT_INTEGOO)
    private String id;
    @SerializedName(COLUMN_USER)
    @DatabaseField(columnName = COLUMN_USER)
    private String user;
    @SerializedName(COLUMN_MESSAGE)
    @DatabaseField(columnName = COLUMN_MESSAGE)
    private String message;
    @SerializedName(COLUMN_DEVICE)
    @DatabaseField(columnName = COLUMN_DEVICE)
    private String device;
    @SerializedName(COLUMN_PLACE)
    @DatabaseField(columnName = COLUMN_PLACE)
    private String place;
    @SerializedName(COLUMN_CREATED)
    @DatabaseField(columnName = COLUMN_CREATED)
    private String created;
    @DatabaseField(columnName = COLUMN_IMAGE, dataType = DataType.BYTE_ARRAY)
    private byte[] image;
    @DatabaseField(foreign = true, canBeNull = true, columnName = COLUMN_IMAGE_ICON_FOREIGN)
    private ImageIcon img;
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_FILTER)
    private Filter filter;

	public long getIdEvent() {
		return idEvent;
	}

	public void setIdEvent(long idEvent) {
		this.idEvent = idEvent;
	}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

	public ImageIcon getImg() {
		return img;
	}

	public void setImg(ImageIcon img) {
		this.img = img;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Event)) {
			return false;
		}
		Event c = (Event) o;
		return c.getId() == getId();
	}

	@Override
	public String toString() {
		return "Event{" +
				"idEvent=" + idEvent +
				", id=" + id +
				", user='" + user + '\'' +
				", message='" + message + '\'' +
				", device='" + device + '\'' +
				", place='" + place + '\'' +
				", created='" + created + '\'' +
				", image=" + Arrays.toString(image) +
				", img=" + img +
				", filter=" + filter +
				'}';
	}
}
