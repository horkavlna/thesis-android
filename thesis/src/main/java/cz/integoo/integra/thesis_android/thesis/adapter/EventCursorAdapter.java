package cz.integoo.integra.thesis_android.thesis.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.model.Event;
import cz.integoo.integra.thesis_android.thesis.support.Support;

/**
 * Created by horkavlna on 08/04/14.
 */
public class EventCursorAdapter extends GeneralCursorAdapter {

	private static final String TAG = EventCursorAdapter.class.getName();

	private LayoutInflater mInflater;
    private Map<String, byte[]> mMapImageIcon;

    public EventCursorAdapter(Context context, Cursor cursorUserList, Map<String, byte[]> mapImageIcon) {
        super(context, cursorUserList, FLAG_REGISTER_CONTENT_OBSERVER);
        //super(context, cursorUserList);
        mInflater = LayoutInflater.from(context);
        this.mMapImageIcon = mapImageIcon;
		this.mHolders = new HashSet<ViewHolder>();
	}

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        GeneralCursorAdapter.ViewHolder viewHolder;
        View viewLayout = mInflater.inflate(R.layout.adapter_event, parent, false);
        viewHolder = new GeneralCursorAdapter.ViewHolder();
        viewHolder.title = (TextView) viewLayout.findViewById(R.id.title);
        viewHolder.subtitle = (TextView) viewLayout.findViewById(R.id.subtitle);
        viewHolder.created = (TextView) viewLayout.findViewById(R.id.created);
        viewHolder.next = (TextView) viewLayout.findViewById(R.id.nextevent);
        viewHolder.eventIcon = (ImageView) viewLayout.findViewById(R.id.icon);
        viewLayout.setTag(viewHolder);
        return viewLayout;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        GeneralCursorAdapter.ViewHolder holder = (GeneralCursorAdapter.ViewHolder) view.getTag();

        if (cursor.getLong(cursor.getColumnIndexOrThrow(Event.COLUMN_ID_EVENT_INTEGOO)) == -1) {
            holder.created.setVisibility(View.GONE);
            holder.subtitle.setVisibility(View.GONE);
            holder.title.setVisibility(View.GONE);
            holder.eventIcon.setVisibility(View.GONE);
            holder.next.setVisibility(View.VISIBLE);
            holder.next.setText(cursor.getString(cursor.getColumnIndexOrThrow(Event.COLUMN_MESSAGE)));
            mHolders.add(holder);
        }  else {
            holder.created.setVisibility(View.VISIBLE);
            holder.subtitle.setVisibility(View.VISIBLE);
            holder.title.setVisibility(View.VISIBLE);
            holder.eventIcon.setVisibility(View.VISIBLE);
            holder.next.setVisibility(View.GONE);

            String idImageIcon = cursor.getString(cursor.getColumnIndexOrThrow(Event.COLUMN_ID_EVENT_INTEGOO));
			Log.i(TAG, "idImageIcon get event:" + idImageIcon);
			if ((mMapImageIcon != null) && (mMapImageIcon.get(idImageIcon) != null)) {
				Log.i(TAG, "image icon:" + idImageIcon + " event:" + cursor.getString(cursor.getColumnIndexOrThrow(Event.COLUMN_MESSAGE)));

				Bitmap bmp = BitmapFactory.decodeByteArray(mMapImageIcon.get(idImageIcon), 0, mMapImageIcon.get(idImageIcon).length);
                holder.eventIcon.setImageBitmap(Bitmap.createScaledBitmap(bmp, 200, 200, false));
            } else {
				//FOR RELEASE REMOVE
				Log.i(TAG, "image icon no exist:" + idImageIcon + " event:" + cursor.getString(cursor.getColumnIndexOrThrow(Event.COLUMN_MESSAGE)));
				holder.eventIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));
			}

            String title = cursor.getString(cursor.getColumnIndexOrThrow(Event.COLUMN_MESSAGE));
            holder.title.setText(title);

            String subtitle = cursor.getString(cursor.getColumnIndexOrThrow(Event.COLUMN_PLACE));
            holder.subtitle.setText(subtitle);

            holder.createdTime = Support.parseISO8601toDate(cursor.getString((cursor.getColumnIndexOrThrow(Event.COLUMN_CREATED))));
            Long durationTime = ((new Date().getTime()/1000) - holder.createdTime);
            holder.created.setText(Support.getDurationTimeWithSeconds(durationTime));

            mHolders.add(holder);
			runUpdateTime();
		}
    }
}
