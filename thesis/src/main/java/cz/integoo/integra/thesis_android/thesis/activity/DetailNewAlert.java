package cz.integoo.integra.thesis_android.thesis.activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.util.Date;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.base.GeneralActivity;
import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.ImageIcon;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.persistence.ApplicationDatabase;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import cz.integoo.integra.thesis_android.thesis.service.ConfirmService;
import cz.integoo.integra.thesis_android.thesis.service.FilterService;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.ToastMessage;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by horkavlna on 05/04/14.
 */
public class DetailNewAlert extends GeneralActivity implements View.OnClickListener, OnRefreshListener {

    private static final String TAG = DetailNewAlert.class.getName();
    public static final String ID_ALERT = "idAlert";
    private ActionBar mActionBar;
    private ApplicationDatabase mDatabase;
    private Alert mAlert;
    private TextView mDuration;
    private TextView mInstruction;
    private ImageView mDetailImage;
    private Button mConfirm;
    private DetailNewAlert mActivity;
    private PersistentData mPersistentData;
    private PullToRefreshLayout mPullToRefreshLayoutDetailNewAlert;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_new_alert);
        mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
        mPersistentData = new PersistentData(getApplicationContext());
        mActivity = this;
        Bundle bundle = getIntent().getExtras();
        if (bundle!= null && bundle.containsKey(ID_ALERT))    {
            mAlert = mDatabase.getAlert(bundle.getLong(ID_ALERT));
                String time = Support.getDurationTimeWithSeconds((new Date().getTime() / 1000) - Support.parseISO8601toDate(mAlert.getCreatedAt()));
                Log.i(TAG, "id Alert:" + mAlert.getId());
				Log.i(TAG, "image Alert:" + mAlert.getImage());
				((TextView) findViewById(R.id.text_device)).setText(mAlert.getDeviceName());
                ((TextView) findViewById(R.id.text_place)).setText(mAlert.getPlaceName());
                ((TextView) findViewById(R.id.text_created)).setText(mAlert.getCreatedAt());
                mDuration = (TextView) findViewById(R.id.text_duration);
                mDuration.setText(time);
                setInstruction();
                mDetailImage = (ImageView) findViewById(R.id.image_alerts);
                setImage();
                mConfirm = (Button) findViewById(R.id.button_confirm_alert);
                mConfirm.setOnClickListener(mActivity);
                setProgressDialog();
        }
        setActionBar();
    }

    public void setActionBar() {
		mActionBar = getActionBar();
		mActionBar.setCustomView(R.layout.actionbar_custom);
		TextView textView = (TextView) mActionBar.getCustomView().findViewById(R.id.title_actionbar);
		textView.setText(mAlert.getName());
		textView.setTextSize(25);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		mActionBar.setLogo(new ColorDrawable(Color.TRANSPARENT));
		mActionBar.setIcon(new ColorDrawable(Color.TRANSPARENT));
		mActionBar.setHomeButtonEnabled(true);
    }

    public void setInstruction() {
        //        Pattern pattern = Pattern.compile("\\/\\/[^\\\"']*[$/\"]");
        String instruction;
        if ((mAlert.getInstruction() != null) || (mAlert.getInstructionDef() != null)) {
            if (mAlert.getInstruction() != null) {
                instruction = mAlert.getInstruction();
            } else {
                instruction = mAlert.getInstructionDef();
            }
            mInstruction = (TextView) findViewById(R.id.text_instruction);
            mInstruction.setText(instruction.replaceAll("//", ""));
            Linkify.addLinks(mInstruction, Linkify.PHONE_NUMBERS);
        }
//        mInstruction = (TextView) findViewById(R.id.text_instruction);
//        mInstruction.setText(mAlert.getInstructions());
//        Linkify.addLinks(mInstruction, pattern, mAlert.getInstructions());
//        mInstruction.setMovementMethod(LinkMovementMethod.getInstance());
        //mInstruction.setText(mAlert.getInstructions());
    }

    public boolean existImage() {
        if (mAlert.getImage() != null) {
            mDetailImage.setOnClickListener(null);
            return true;
        } else {
            mDetailImage.setOnClickListener(mActivity);
            return false;
        }
    }

    public void setImage() {
		if (existImage()) {
			ByteArrayInputStream imageStream = new ByteArrayInputStream(mAlert.getImage());
			Bitmap bmp = BitmapFactory.decodeStream(imageStream);
			mDetailImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, 200, 200, false));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
        if (mAlert!= null && mDuration != null) {
            runUpdateTime(mDuration, mAlert.getCreatedAt());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ApplicationDatabase.closeManaged();
        stopUpdateTime();
    }

    @Override
    public void onClick(View v) {
        String urlAddress = null;
        String method = null;
        Intent intentService = null;
        if (v.getClass() == ImageView.class) {
            Log.i(TAG, "onClick image detail");
            urlAddress = Support.URL_ALERTS + "/" + mAlert.getId() + Support.URL_IMAGE;
            method = Support.GET;
            intentService = new Intent(mActivity, FilterService.class);
            startAnimation();
//            connectionToServer(urlAddress, method, intentService);
        } else if (v.getClass() == Button.class) {
            Log.i(TAG, "onClick confirm");
            urlAddress = Support.URL_ALERT_CONFIRM + mAlert.getId();
            method = Support.POST;
            intentService = new Intent(mActivity, ConfirmService.class);
            mConfirm.setEnabled(false);
            mDetailImage.setOnClickListener(null);
            showInstruction(urlAddress, method, intentService);
        }
    }

    public void connectionToServer(String urlAddress, String method, Intent intentService) {
        Log.i(TAG, "prepare sending (confirm/image) on the server");
        if (mActivity != null && urlAddress != null && method != null && intentService != null) {
            if (Support.checkNetwork(mActivity)) {
                if (mDatabase.isOpen()) {
                    User user = mDatabase.getUser(mPersistentData.getSignedUserName());
                    intentService.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerNewAlert));
                    intentService.putExtra(Support.EXTRA_URL, mPersistentData.getServerAddress() + urlAddress);
                    intentService.putExtra(Support.EXTRA_METHOD, method);
                    //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                    intentService.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                    intentService.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                    intentService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mActivity.startService(intentService);
                }
            } else {
                mPullToRefreshLayoutDetailNewAlert.setRefreshing(false);
                mConfirm.setEnabled(true);
                existImage();
                showErrorMessage(mActivity.getString(R.string.alert_messsage_no_internet_connection));
            }
        }
    }

    public void showInstruction(final String urlAddress, final String method, final Intent intentService) {
        final View layoutDialog = getLayoutInflater().inflate(R.layout.dialog_instruction, null);
        final EditText instruction = (EditText) layoutDialog.findViewById(R.id.instruction);
        instruction.setText(mPersistentData.getDefaultUser());
        instruction.selectAll();
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setView(layoutDialog);
        builder.setTitle(mActivity.getString(R.string.alert_instruction));
        builder.setPositiveButton(R.string.alert_instruction_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                intentService.putExtra(Support.EXTRA_BODY, instruction.getText().toString());
                InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layoutDialog.getWindowToken(),0);
                mPullToRefreshLayoutDetailNewAlert.setRefreshing(true);
                connectionToServer(urlAddress, method, intentService);
            }
        });
        builder.setNegativeButton(R.string.preference_settings_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layoutDialog.getWindowToken(),0);
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void startAnimation() {
        Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        mDetailImage.startAnimation(rotate);
        mConfirm.setEnabled(false);
        mDetailImage.setOnClickListener(null);
    }

    public void stopAnimation() {
        mDetailImage.setAnimation(null);
    }

    private Handler handlerNewAlert = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                mDetailImage.setOnClickListener(mActivity);
                mConfirm.setEnabled(true);
                Log.i(TAG, "handleMessage from Toast");
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage != null) {
                showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
                mConfirm.setEnabled(false);
                mPullToRefreshLayoutDetailNewAlert.setRefreshing(false);
                stopAnimation();
            } else {
                if (bundle.getInt(Support.EXTRA_RESPONSE_STATUSCODE) == 204) {
                    mPullToRefreshLayoutDetailNewAlert.setRefreshing(false);
                    mConfirm.setOnClickListener(mActivity);
                    //confirm alert
                    Log.i(TAG, "confirm alert");
                    Intent intent = new Intent();
                    intent.setClassName(getApplicationContext(), MenuAcvitity.class.getName());
                    startActivity(intent);
                    mDatabase.removeAlert(mAlert);
                } else {
					Log.i(TAG, "get image alert");
					byte[] imateByte = bundle.getByteArray(Support.EXTRA_RESPONSE_BODY);
                    //SAVE TO THE DATABASE
					ImageIcon parseImage = Support.parseByteToImage(imateByte);
					mAlert.setImage(parseImage.getImg());
                    mDatabase.updateAlertImage(mAlert);
                    stopAnimation();
                    mDetailImage.setOnClickListener(null);
                    mConfirm.setEnabled(true);
                    setImage();
                }
            }
        }
    };

    public void setProgressDialog() {
        mPullToRefreshLayoutDetailNewAlert = ((PullToRefreshLayout) mActivity.findViewById(R.id.ptr_layout));
        ActionBarPullToRefresh.from(this)
                // Mark All Children as pullable
                .allChildrenArePullable()
                        // Set a OnRefreshListener
                .listener(this)
                        // Finally commit the setup to our PullToRefreshLayout
                .setup(mPullToRefreshLayoutDetailNewAlert);
    }

    public void showErrorMessage(String message) {
		new ToastMessage(mActivity, message, handlerNewAlert, R.id.layout_detail);
    }

    @Override
    public void onRefreshStarted(View view) {

    }
}
