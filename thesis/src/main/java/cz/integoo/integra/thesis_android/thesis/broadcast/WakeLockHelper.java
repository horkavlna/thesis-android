package cz.integoo.integra.thesis_android.thesis.broadcast;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

/**
 * Created by horkavlna on 05/05/14.
 */
public class WakeLockHelper {
    private static final String TAG = WakeLockHelper.class.getName();
    public static final String LOCK_NAME = "cz.integoo.integra.thesis_android.thesis.wakelock";
    private static PowerManager.WakeLock sWakeLock = null;

    public static synchronized void lock(Context context) {
        Log.i(TAG, "lock");
        if (sWakeLock == null) {
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            sWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WakeLockHelper.LOCK_NAME);
            sWakeLock.acquire();
        }
    }

    public static synchronized boolean isHeald() {
        return sWakeLock.isHeld();
    }

    public static synchronized void release() {
        Log.i(TAG, "release");
        if (sWakeLock != null) {
            sWakeLock.release();
        }
    }
}
