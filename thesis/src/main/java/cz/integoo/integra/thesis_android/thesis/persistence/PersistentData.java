package cz.integoo.integra.thesis_android.thesis.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import cz.integoo.integra.thesis_android.thesis.R;

/**
 * Created by horkavlna on 25/03/14.
 */
public class PersistentData {

    public static final String SHARED_PREFERENCES_NAME = "cz.integoo.integra.thesis.sharedpreferences";

    private static final String ALARM_PUSH_NOTIFICATION = "AlarmPushNotification";
    private static final String SERVER_ADDRESS = "ServerAddress";
    private static final String FETCH_LIMIT = "FetchLimit";
	private static final String AUTOMATIC_FETCH_TIME = "AutomaticFetchTime";
	private static final String DEFAULT_USER = "DefaultUser";
    private static final String TIME_AUTOMATIC_LOGOUT = "TimeAutomaticLogOut";
    private static final String LAST_TIME_IN_APPLICATION = "LastTimeInApplication";
    private static final String SIGNIN_USER_NAME = "SigninUserName";
    private static final String REGIDGCM = "regidgcm";
    private static final String FIRST_TIME_CALL_BROADCAST_RECEIVER = "firstTimeCallBroadcastReceiver";

    //SETTINGS KEY
    public static final String KEY_REMOTE_PUSH_NOTIFICATION = "remote_push_notification";

    private final SharedPreferences SHAREDPREFERENCES;
    private Editor mEditor;
    private Context context;

    public PersistentData(Context context) {
        this.context = context;
        this.SHAREDPREFERENCES = context.getSharedPreferences(PersistentData.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private Editor getEditor() {
        if (mEditor == null) {
            mEditor = SHAREDPREFERENCES.edit();
        }
        return mEditor;
    }

    public void apply() {
        getEditor().apply();
    }

    public Boolean getAlarmPushNotification() {
        return SHAREDPREFERENCES.getBoolean(ALARM_PUSH_NOTIFICATION, true);
    }

    public void putAlarmPushNotification(Boolean alarmPushNotification) {
        getEditor().putBoolean(ALARM_PUSH_NOTIFICATION, alarmPushNotification);
    }

    public Boolean getFirstTimeCallBroadcastReceiver() {
        return SHAREDPREFERENCES.getBoolean(FIRST_TIME_CALL_BROADCAST_RECEIVER, true);
    }

    public void putFirstTimeCallBroadcastReceiver(Boolean alarmPushNotification) {
        getEditor().putBoolean(FIRST_TIME_CALL_BROADCAST_RECEIVER, alarmPushNotification);
    }

    public String getServerAddress() {
        return SHAREDPREFERENCES.getString(SERVER_ADDRESS, context.getResources().getString(R.string.default_server_address));
    }

    public void putServerAddress(String serverAddress) {
        getEditor().putString(SERVER_ADDRESS, serverAddress);
    }

    public Integer getFetchLimit() {
        return SHAREDPREFERENCES.getInt(FETCH_LIMIT, Integer.valueOf(context.getResources().getString(R.string.default_fetch_limit)));
    }

    public void putFetchLimit(Integer fetchLimit) {
        getEditor().putInt(FETCH_LIMIT, fetchLimit);
    }

	public long getAutomaticFetchTime() {
		return SHAREDPREFERENCES.getLong(AUTOMATIC_FETCH_TIME, Long.valueOf(context.getResources().getString(R.string.default_automatic_fetch_time)));
	}

	public void putAutomaticFetchTime(Long fetchLimit) {
		getEditor().putLong(AUTOMATIC_FETCH_TIME, fetchLimit);
	}

    public String getDefaultUser() {
        return SHAREDPREFERENCES.getString(DEFAULT_USER, null);
    }

    public void putDefaultUser(String defaultUser) {
        getEditor().putString(DEFAULT_USER, defaultUser);
    }

    public long getTimeLogout() {
        return SHAREDPREFERENCES.getLong(TIME_AUTOMATIC_LOGOUT, 0);
    }

    public void putTimeLogout(long timeLogout) {
        getEditor().putLong(TIME_AUTOMATIC_LOGOUT, timeLogout);
    }

    public long getLastTimeInApplication() {
        return SHAREDPREFERENCES.getLong(LAST_TIME_IN_APPLICATION, 0L);
    }

    public void putLastTimeInApplication(long lastTime) {
        getEditor().putLong(LAST_TIME_IN_APPLICATION, lastTime);
    }

    public String getSignedUserName() {
        return SHAREDPREFERENCES.getString(SIGNIN_USER_NAME, null);
    }

    public void putSignedUserName(String user) {
        getEditor().putString(SIGNIN_USER_NAME, user);
    }

    public String getRegIdGcm() {
        return SHAREDPREFERENCES.getString(REGIDGCM, null);
    }

    public void putRegIdGcm(String regIdGcm) {
        getEditor().putString(REGIDGCM, regIdGcm);
    }

    public boolean isPreferedRemotePushNotification() {
        return SHAREDPREFERENCES.getBoolean(KEY_REMOTE_PUSH_NOTIFICATION, true);
    }

    public void clearPersistentData() {
        putSignedUserName(null);
        putRegIdGcm(null);
    }
}
