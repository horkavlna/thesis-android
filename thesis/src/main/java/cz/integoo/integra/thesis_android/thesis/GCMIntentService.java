package cz.integoo.integra.thesis_android.thesis;

/**
 * Created by horkavlna on 26/04/14.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

import cz.integoo.integra.thesis_android.thesis.activity.DetailNewAlert;
import cz.integoo.integra.thesis_android.thesis.activity.MenuAcvitity;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import de.greenrobot.event.EventBus;

public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";
    public static final String SENDER_ID = "266877801682";

    public GCMIntentService() {
        super(SENDER_ID);
    }

    @Override
    protected void onError(Context context, String arg1) {
        Log.d(TAG, "error " + arg1.toString());
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message from remote push notification");
        String messageAlert = intent.getStringExtra("alert");
        String idAlert = intent.getStringExtra("acme");
        Long idAlertNumber = null;
        try{
            idAlertNumber = Long.parseLong(idAlert);
        } catch (NumberFormatException ex) {
            idAlertNumber = -1l;
        }
        Log.i(TAG, "messageAlert GCM: " +messageAlert);
        Log.i(TAG, "idAlert GCM: " +idAlert);
        PersistentData persistentData = new PersistentData(context);
        if (persistentData.isPreferedRemotePushNotification()) {
            generateNotification(context, messageAlert, idAlertNumber);
        }
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "onDeletedMessages");
    }


    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "registered " + registrationId);
        EventBus.getDefault().post(registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "unregistered " + registrationId.toString());
    }

    /**
     * Create a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message, Long idAlert) {
        Log.i(TAG, "show notification");
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
        context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);

        String title = context.getString(R.string.app_name);

        Intent notificationIntent = new Intent(context, MenuAcvitity.class);
        // set intent so it does not start a new activity
        notificationIntent.putExtra(DetailNewAlert.ID_ALERT, idAlert);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);
    }
}
