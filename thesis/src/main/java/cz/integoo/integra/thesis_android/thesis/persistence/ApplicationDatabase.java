package cz.integoo.integra.thesis_android.thesis.persistence;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.AndroidDatabaseResults;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;
import cz.integoo.integra.thesis_android.thesis.model.Event;
import cz.integoo.integra.thesis_android.thesis.model.Filter;
import cz.integoo.integra.thesis_android.thesis.model.ImageIcon;
import cz.integoo.integra.thesis_android.thesis.model.User;

/**
 * Created by horkavlna on 29/03/14.
 */
public class ApplicationDatabase extends OrmLiteSqliteOpenHelper {
    private static final String TAG = ApplicationDatabase.class.getName();
    private static final String DATABASE_NAME = "IntegooAppDB";
    private static final int DATABASE_VERSION = 1;

    private Context context;

    private static final Class<?>[] entities = {
            User.class,
            Filter.class,
            Event.class,
            Alert.class,
            ImageIcon.class
    };

    public ApplicationDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i(TAG, " ApplicationDatabase ");
        this.context = context;
    }

    public static ApplicationDatabase createManaged(Context context) {
        Log.d(TAG, "open database");
        return OpenHelperManager.getHelper(context, ApplicationDatabase.class);
    }

    public static void closeManaged() {
        Log.d(TAG, "close database");
        OpenHelperManager.releaseHelper();
    }

    private void createTables(ConnectionSource connectionSource) {
        for (Class<?> entity : entities) {
            try {
                TableUtils.createTableIfNotExists(connectionSource, entity);
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private void dropTables(ConnectionSource connectionSource) {
        try {
            for (Class<?> entity : entities) {
                TableUtils.dropTable(connectionSource, entity, true);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        createTables(connectionSource);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        dropTables(connectionSource);
        createTables(connectionSource);
    }

    public Cursor getCursor(Class<?> c) {
        try {
            Dao o = getDao(c);
            Dao<?, Long> dao = (Dao<?, Long>) o; //hack for maven build generic resolving
            CloseableIterator<?> iterator = dao.iterator();
            return getCursor(iterator);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public long getExistImageIcon(Class<?> c, String userName) {
        try {
            Dao o = getDao(c);
            QueryBuilder queryBuilder = o.queryBuilder();
            queryBuilder.where().eq(ImageIcon.COLUMN_USER, getUser(userName));
            return  queryBuilder.query().size();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Cursor getCursorAlert(Class<?> c, String userName, AlertStatus isNewAlert) {
        try {
            Dao o = getDao(c);
            QueryBuilder queryBuilder = o.queryBuilder();
            queryBuilder.orderBy(Alert.COLUMN_CREATED_AT, true);
            queryBuilder.where().eq(Alert.COLUMN_STATUS, isNewAlert).and().eq(Alert.COLUMN_USER, getUser(userName));
            CloseableIterator<?> iterator = queryBuilder.iterator();
            return getCursor(iterator);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Cursor getCursorEvent(Class<?> c, Filter filter) {
        try {
            Dao o = getDao(c);
            QueryBuilder queryBuilder = o.queryBuilder();
            queryBuilder.orderBy(Event.COLUMN_CREATED, true);
            queryBuilder.where().eq(Event.COLUMN_FILTER, filter);
            CloseableIterator<?> iterator = queryBuilder.iterator();
            return getCursor(iterator);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Cursor getCursorFilter(Class<?> c, String userName) {
        try {
            Dao o = getDao(c);
            QueryBuilder queryBuilder = o.queryBuilder();
            queryBuilder.where().eq(Filter.COLUMN_USER, getUser(userName));
            CloseableIterator<?> iterator = queryBuilder.iterator();
            return getCursor(iterator);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Cursor getCursor(Collection<?> c) {
        if (c instanceof ForeignCollection) {
            ForeignCollection<?> fc = (ForeignCollection<?>) c;
            return getCursor(fc.closeableIterator());
        }
        throw new IllegalArgumentException("Collection must be ForeignCollection implementation obtained from entity!");
    }

    private Cursor getCursor(CloseableIterator<?> iterator) {
        AndroidDatabaseResults results = (AndroidDatabaseResults) iterator.getRawResults();
        return results.getRawCursor();
    }

    private <T> Cursor getCursor(Class<T> c, PreparedQuery<T> preparedQuery) {
        try {
            Dao o = getDao(c);
            Dao<T, Long> dao = (Dao<T, Long>) o;
            //Dao<T, Long> dao = getDao(c);
            CloseableIterator<?> iterator = dao.iterator(preparedQuery);
            AndroidDatabaseResults results = (AndroidDatabaseResults) iterator.getRawResults();
            return results.getRawCursor();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    // ------------------------------------------------------------------------
    // USER
    // ------------------------------------------------------------------------
    public void saveUser(User user) {
        try {
            Dao o = getDao(User.class);
            Dao<User, Long> userDao = (Dao<User, Long>) o;
            userDao.createOrUpdate(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User getUser(String username) {
        try {
            Dao o = getDao(User.class);
            Dao<User, Long> userDao = (Dao<User, Long>) o;
            List<User> listUsers = userDao.queryForEq(User.COLUMN_USERNAME,username);
            if (!listUsers.isEmpty()) {
                return listUsers.get(0);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void removeUser(User user) {
        try {

            Dao o = getDao(User.class);
            Dao<User, Long> dao = (Dao<User, Long>) o;
            dao.delete(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

	public void removeAllUser() {
		try {
			Dao o = getDao(User.class);
			Dao<User, Long> userDao = (Dao<User, Long>) o;
			List<User> listUser = userDao.queryForAll();
			for (User user : listUser) {
				removeUser(user);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

    // ------------------------------------------------------------------------
    // ALERTS
    // ------------------------------------------------------------------------
    public void saveAlerts(List<Alert> listAlert, User user, AlertStatus status) {
        try {
            Dao o = getDao(Alert.class);
            Dao<Alert, Long> alertDao = (Dao<Alert, Long>) o;
            for (Alert alert : listAlert) {
					alert.setUser(user);
					alert.setStatus(status);
				if (existAlertForUser(user, alert)) {
					alertDao.create(alert);
				}
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

	public boolean existAlertForUser(User user, Alert alert) {
		return !user.getAlert().contains(alert);
	}

    public void updateAlertImage(Alert alert) {
        try {
            Dao o = getDao(Alert.class);
            Dao<Alert, Long> alertDao = (Dao<Alert, Long>) o;
            alertDao.createOrUpdate(alert);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Alert getAlert(long alertId) {
        try {
            Dao o = getDao(Alert.class);
            Dao<Alert, Long> dao = (Dao<Alert, Long>) o;
			List<Alert> listAlert = dao.queryForEq(Alert.COLUMN_ID_ALERT_INTEGOO, alertId);
            if (!listAlert.isEmpty()) {
				return listAlert.get(0);
			}
			return  null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void removeAlert(Alert alert) {
        try {
            Dao o = getDao(Alert.class);
            Dao<Alert, Long> dao = (Dao<Alert, Long>) o;
            dao.delete(alert);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    // ------------------------------------------------------------------------
    // EVENT
    // ------------------------------------------------------------------------

    public void saveEvent(List<Event> listEvent, Filter filter) {
        try {
            Dao o = getDao(Event.class);
            Dao<Event, Long> eventDao = (Dao<Event, Long>) o;
            for (Event event: listEvent) {
					event.setFilter(filter);
					event.setImg(getImageIcon(event.getId()));
				if (existEventForUser(filter, event)) {
					eventDao.create(event);
				}
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

	public boolean existEventForUser(Filter filter, Event event) {
		return !filter.getEvents().contains(event);
	}

	public void updateEventImage(Event event) {
        try {
            Dao o = getDao(Event.class);
            Dao<Event, Long> eventDao = (Dao<Event, Long>) o;
            eventDao.createOrUpdate(event);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Event getEvent(String eventId) {
        try {
			Dao o = getDao(Event.class);
			Dao<Event, Long> dao = (Dao<Event, Long>) o;
			List<Event> listEvent = dao.queryForEq(Event.COLUMN_ID_EVENT_INTEGOO, eventId);
			if (!listEvent.isEmpty()) {
				return listEvent.get(0);
			}
			return  null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void removeEvent(Event event) {
        try {
            Dao o = getDao(Event.class);
            Dao<Event, Long> dao = (Dao<Event, Long>) o;
            dao.delete(event);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // ------------------------------------------------------------------------
    // FILTER
    // ------------------------------------------------------------------------

    public void saveFilter(List<Filter> listFilter, User user) {
        try {
            Dao o = getDao(Filter.class);
            Dao<Filter, Long> filterDao = (Dao<Filter, Long>) o;
            for (Filter filter: listFilter) {
				//filter dosn't exist
				filter.setUser(user);
				if (existFilterForUser(user, filter)) {
					filterDao.create(filter);
				}
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

	public boolean existFilterForUser(User user, Filter filter) {
		return !user.getFilter().contains(filter);
	}

	public Filter getFilterIntegooId(long idFilter) {
		try {
			Dao o = getDao(Filter.class);
			Dao<Filter, Long> userDao = (Dao<Filter, Long>) o;
			Map<String, Object> queryValues = new HashMap<String, Object>();
			queryValues.put(Filter.COLUMN_ID_INTEGOO, idFilter);
			List<Filter> listFilter = userDao.queryForFieldValues(queryValues);
			if (!listFilter.isEmpty()) {
				return listFilter.get(0);
			}
			return null;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

    public Filter getFilterId(long idFilter) {
        try {
            Dao o = getDao(Filter.class);
            Dao<Filter, Long> userDao = (Dao<Filter, Long>) o;
            Map<String, Object> queryValues = new HashMap<String, Object>();
            queryValues.put(Filter.COLUMN_ID_FILTER, idFilter);
            List<Filter> listFilter = userDao.queryForFieldValues(queryValues);
            if (!listFilter.isEmpty()) {
                return listFilter.get(0);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // ------------------------------------------------------------------------
    // IMAGE ICON
    // ------------------------------------------------------------------------

    public void saveImageIcon(List<ImageIcon> listImageIcon, String userName) {
        try {
            Dao o = getDao(ImageIcon.class);
            Dao<ImageIcon, Long> imageIconDao = (Dao<ImageIcon, Long>) o;
            for (ImageIcon imageIcon: listImageIcon) {
                imageIcon.setUser(getUser(userName));
                imageIconDao.createOrUpdate(imageIcon);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ImageIcon getImageIcon(String imageIcon) {
        try {
            Dao o = getDao(ImageIcon.class);
            Dao<ImageIcon, Long> userDao = (Dao<ImageIcon, Long>) o;
            List<ImageIcon> listImageIcon = userDao.queryForEq(ImageIcon.COLUMN_ID_INTEGOO, imageIcon);
            if (!listImageIcon.isEmpty()) {
                return listImageIcon.get(0);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, byte[]> getAllImageIcon() {
        try {
            Dao o = getDao(ImageIcon.class);
            Dao<ImageIcon, Long> userDao = (Dao<ImageIcon, Long>) o;
            List<ImageIcon> listImageIcon = userDao.queryForAll();
            Map<String, byte[]> mapImageIcon = new HashMap<String, byte[]>();
            if (!listImageIcon.isEmpty()) {
                for (ImageIcon icon : listImageIcon) {
					Log.i(TAG, " image icon from db:" + icon.getId() + " icon image:" + icon.getImg());
					mapImageIcon.put(icon.getId(), icon.getImg());
                }
                return mapImageIcon;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
