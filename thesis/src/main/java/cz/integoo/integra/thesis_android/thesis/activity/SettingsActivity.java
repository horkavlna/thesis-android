package cz.integoo.integra.thesis_android.thesis.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.TextView;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.base.GeneralPreferenceActivity;
import cz.integoo.integra.thesis_android.thesis.fragment.SettingsFragment;
import cz.integoo.integra.thesis_android.thesis.persistence.ApplicationDatabase;

/**
 * Created by horkavlna on 25/03/14.
 */
public class SettingsActivity extends GeneralPreferenceActivity {

    private static Menu mMenu;
	private static boolean denyBackButton;
    private ApplicationDatabase mDatabase;
	private ActionBar mActionBar;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
		denyBackButton = false;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar, menu);
        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_done).setVisible(false);
		mActionBar = getActionBar();
		mActionBar.setCustomView(R.layout.actionbar_custom);
		TextView textView = (TextView) mActionBar.getCustomView().findViewById(R.id.title_actionbar);
		textView.setText(getApplicationContext().getString(R.string.preference_settings_title));
		textView.setTextSize(25);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		mActionBar.setLogo(new ColorDrawable(Color.TRANSPARENT));
		mActionBar.setIcon(new ColorDrawable(Color.TRANSPARENT));
		mActionBar.setHomeButtonEnabled(true);
		allowBackButton();
		return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onStop() {
        super.onStop();
        ApplicationDatabase.closeManaged();
    }

    public void logOutUserStartActivity() {
        Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setClassName(this, SigninActivity.class.getName());
        startActivity(intent);
    }

	@Override
	public void onBackPressed() {
		if (!denyBackButton) {
			super.onBackPressed();
		}
	}

	public void denyBackButton() {
		mActionBar.setDisplayHomeAsUpEnabled(false);
		denyBackButton = true;
	}

	public void allowBackButton() {
		mActionBar.setDisplayHomeAsUpEnabled(true);
		denyBackButton = false;
	}

    public Menu getmMenu() {
        return mMenu;
    }

    public void setmMenu(Menu mMenu) {
        this.mMenu = mMenu;
    }

    public ApplicationDatabase getmDatabase() {
        return mDatabase;
    }

    public void setmDatabase(ApplicationDatabase mDatabase) {
        this.mDatabase = mDatabase;
    }
}
