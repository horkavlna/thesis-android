package cz.integoo.integra.thesis_android.thesis.activity.base;

import android.preference.PreferenceActivity;
import android.util.Log;

/**
 * Created by horkavlna on 04/05/14.
 */
public class GeneralPreferenceActivity extends PreferenceActivity {

    private static final String TAG = GeneralPreferenceActivity.class.getName();
    private static boolean isAppWentToBackground = true;
    private static boolean isWindowFocused = false;
    private static boolean isBackPressed = false;

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart isAppWentToBackground " + isAppWentToBackground);
        applicationWillEnterForeground();
        super.onStart();
    }

    private void applicationWillEnterForeground() {
        if (isAppWentToBackground && GeneralActivity.isIsAppWentToBackground() && GeneralFragmentActivity.isIsAppWentToBackground()) {
            isAppWentToBackground = false;
            GeneralActivity.logOutUser(getApplicationContext());
//            Toast.makeText(getApplicationContext(), "App is in foreground", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop ");
        applicationdidenterbackground();
    }

	public void applicationdidenterbackground() {
        if (!isWindowFocused && !GeneralActivity.isIsWindowFocused() && !GeneralFragmentActivity.isIsWindowFocused()) {
            isAppWentToBackground = true;
            GeneralActivity.setIsAppWentToBackground(true);
            GeneralFragmentActivity.setIsAppWentToBackground(true);
            GeneralActivity.saveLastTimeInApplication(getApplicationContext());
//            Toast.makeText(getApplicationContext(), "App is Going to Background", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (this instanceof GeneralPreferenceActivity) {
        } else {
            isBackPressed = true;
        }
        Log.d(TAG, "onBackPressed " + isBackPressed + "" + this.getLocalClassName());
        super.onBackPressed();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        isWindowFocused = hasFocus;
        Log.i(TAG, "onWindowFocusChanged " + isWindowFocused);
        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public static boolean isIsBackPressed() {
        return isBackPressed;
    }

    public static boolean isIsAppWentToBackground() {
        return isAppWentToBackground;
    }

    public static boolean isIsWindowFocused() {
        return isWindowFocused;
    }

    public static void setIsAppWentToBackground(boolean isAppWentToBackground) {
        GeneralPreferenceActivity.isAppWentToBackground = isAppWentToBackground;
    }
}
