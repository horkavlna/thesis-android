package cz.integoo.integra.thesis_android.thesis.support;

/**
 * Created by horkavlna on 30/03/14.
 */
public interface DetailListViewListener {

    public void onNewAlertDetailSelected(long idAlert);
    public void onExistAlertDetailSelected(long idAlert);
    public void onEventDetailSelected(String idEvent);
    public void onFilterSelected();
}
