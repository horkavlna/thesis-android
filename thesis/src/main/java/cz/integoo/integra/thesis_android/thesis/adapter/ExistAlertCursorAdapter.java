package cz.integoo.integra.thesis_android.thesis.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Date;
import java.util.HashSet;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.support.Support;

/**
 * Created by horkavlna on 08/04/14.
 */
public class ExistAlertCursorAdapter extends GeneralCursorAdapter {

    private LayoutInflater mInflater;

    public ExistAlertCursorAdapter(Context context, Cursor cursorUserList) {
        super(context, cursorUserList, FLAG_REGISTER_CONTENT_OBSERVER);
        //super(context, cursorUserList);
        mInflater = LayoutInflater.from(context);
		this.mHolders = new HashSet<ViewHolder>();
	}

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        GeneralCursorAdapter.ViewHolder viewHolder;
        View viewLayout = mInflater.inflate(R.layout.adapter_exist_alert, parent, false);
        viewHolder = new GeneralCursorAdapter.ViewHolder();
        viewHolder.relativeLayout = (RelativeLayout) viewLayout.findViewById(R.id.row);
        viewHolder.title = (TextView) viewLayout.findViewById(R.id.title);
        viewHolder.subtitle = (TextView) viewLayout.findViewById(R.id.subtitle);
        viewHolder.created = (TextView) viewLayout.findViewById(R.id.created);
        viewHolder.next = (TextView) viewLayout.findViewById(R.id.nextalert);
        viewLayout.setTag(viewHolder);
        return viewLayout;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        GeneralCursorAdapter.ViewHolder holder = (GeneralCursorAdapter.ViewHolder) view.getTag();

        if (cursor.getLong(cursor.getColumnIndexOrThrow(Alert.COLUMN_ID_ALERT_INTEGOO)) == -1) {
            holder.relativeLayout.setBackgroundColor(Color.WHITE);
            holder.created.setVisibility(View.GONE);
            holder.subtitle.setVisibility(View.GONE);
            holder.title.setVisibility(View.GONE);
            holder.next.setVisibility(View.VISIBLE);
            holder.next.setText(cursor.getString(cursor.getColumnIndexOrThrow(Alert.COLUMN_NAME)));
            mHolders.add(holder);
        }  else {
            holder.created.setVisibility(View.VISIBLE);
            holder.subtitle.setVisibility(View.VISIBLE);
            holder.title.setVisibility(View.VISIBLE);
            holder.next.setVisibility(View.GONE);
            String color = cursor.getString(cursor.getColumnIndexOrThrow(Alert.COLUMN_TYPE_COLOR));
            holder.relativeLayout.setBackgroundColor(Color.parseColor(color));
			holder.relativeLayout.getBackground().setAlpha(Support.OPAQUE_COLOR);

			String title = cursor.getString(cursor.getColumnIndexOrThrow(Alert.COLUMN_NAME));
            holder.title.setText(title);

            String subtitle = cursor.getString(cursor.getColumnIndexOrThrow(Alert.COLUMN_PLACE_NAME));
            holder.subtitle.setText(subtitle);

            holder.createdTime = Support.parseISO8601toDate(cursor.getString((cursor.getColumnIndexOrThrow(Alert.COLUMN_CREATED_AT))));
            Long durationTime = ((new Date().getTime()/1000) - holder.createdTime);
            holder.created.setText(Support.getDurationTimeWithSeconds(durationTime));

            mHolders.add(holder);
			runUpdateTime();
		}
    }
}
