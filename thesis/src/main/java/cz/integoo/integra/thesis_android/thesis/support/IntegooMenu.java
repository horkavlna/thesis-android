package cz.integoo.integra.thesis_android.thesis.support;

import android.app.Application;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by horkavlna on 28/05/14.
 */
public class IntegooMenu extends Application {
	private static MenuItem menuItem;
	private static Menu menu;

	public static MenuItem getMenuItem() {
		return menuItem;
	}

	public static void setMenuItem(MenuItem menuItem) {
		IntegooMenu.menuItem = menuItem;
	}

	public static Menu getMenu() {
		return menu;
	}

	public static void setMenu(Menu menu) {
		IntegooMenu.menu = menu;
	}
}
