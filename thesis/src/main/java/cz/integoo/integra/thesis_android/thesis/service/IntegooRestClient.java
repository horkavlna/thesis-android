package cz.integoo.integra.thesis_android.thesis.service;

import android.app.DownloadManager;
import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;

/**
 * Created by horkavlna on 28/03/14.
 */
public class IntegooRestClient {

    private AsyncHttpClient mClient;
    public IntegooRestClient(String username, String password) {
        mClient = new AsyncHttpClient();
        mClient.setTimeout(Support.CONNECTION_TIMEOUT);
        mClient.setMaxRetriesAndTimeout(1, 4000);
		mClient.setBasicAuth(username,password);
    }
    public void get(Context context, String url, RequestParams requestParam, HashMap<String, String> header, AsyncHttpResponseHandler responseHandler) {
        if (header != null) {
            setHttpHeader(header);
        }
        mClient.get(context, url, requestParam, responseHandler);
    }

    public void post(Context context, String url, HashMap<String, String> header, AsyncHttpResponseHandler responseHandler) {
        if (header != null) {
            setHttpHeader(header);
        }
        mClient.post(context, url, null, responseHandler);
    }

    public void post(Context context, String url, HashMap<String, String> header, String body, String contentType, AsyncHttpResponseHandler responseHandler) {
        if (header != null) {
            setHttpHeader(header);
        }
        mClient.post(context, url, setBody(body), contentType, responseHandler);
    }

    public void delete(Context context, String url, AsyncHttpResponseHandler responseHandler) {
        mClient.delete(context, url, responseHandler);
    }

	public void cancel(Context context) {
		mClient.cancelRequests(context, true);
	}

    private StringEntity setBody(String body) {
        if (body == null) {
            return null;
        }
        StringEntity entity;
        try {
            entity = new StringEntity(body);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }

    private void setHttpHeader(HashMap<String, String> header) {
        Iterator it = header.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            mClient.addHeader((String) pairs.getKey(), String.valueOf(pairs.getValue()));
        }
    }

}
