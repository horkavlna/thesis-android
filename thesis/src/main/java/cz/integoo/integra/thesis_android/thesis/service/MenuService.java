package cz.integoo.integra.thesis_android.thesis.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.Event;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;

/**
 * Created by horkavlna on 30/03/14.
 */
public class MenuService extends IntentService {

    private static final String TAG = MenuService.class.getName();

    private boolean success;

    public MenuService() {
        super("alert service");
    }

    public MenuService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, " onHandleIntent ");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.getBoolean(Support.EXTRA_IS_ALERT, true)) {
                new MenuAsyncTask(new ArrayList<Alert>(), bundle).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
            } else  {
                new MenuAsyncTask(new ArrayList<Event>(), bundle, null).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
            }
        }
    }

    @Override
    public boolean stopService(Intent name) {
        Log.d(TAG, " stopService ");
        return super.stopService(name);
    }

    //ASYNCTASK FOR CONNECTION
    private class MenuAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private Messenger mMessanger;
        private AlertHandler mHandler;
        private ArrayList<Alert> mAlertList;
        private ArrayList<Event> mEventList;
        private Bundle mBundle;
		private IntegooRestClient mIntegooRestClient;

		private MenuAsyncTask(ArrayList<Alert> alertList, Bundle bundle) {
            this.mAlertList = alertList;
            this.mBundle = bundle;
        }

        private MenuAsyncTask(ArrayList<Event> eventList, Bundle bundle, Object o) {
            this.mEventList = eventList;
            this.mBundle = bundle;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.i(TAG, " doInBackground url:" + mBundle.get(Support.EXTRA_URL));
            Iterator it = ((HashMap) mBundle.get(Support.EXTRA_HEADER)).entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry)it.next();
                Log.i(TAG, "header request key:" + pairs.getKey() + " value:" + pairs.getValue());
            }
            this.mMessanger = (Messenger) mBundle.get(Support.EXTRA_HANDLER);
            this.mHandler = new AlertHandler();

            RequestParams requestParams = new RequestParams();
            if (mBundle.containsKey(Support.EXTRA_REQUEST_PARAM)) {
                UrlRequestParam urlRequestParam = mBundle.getParcelable(Support.EXTRA_REQUEST_PARAM);
                if (mBundle.getBoolean(Support.EXTRA_IS_ALERT, true)) {
                    requestParams.put(Support.URL_REQUEST_PARAM_STATUS, urlRequestParam.getAlertStatus());
                }
                requestParams.add(Support.URL_REQUEST_PARAM_FIELDS, urlRequestParam.getFields());
            }
            return createHttpConnection((String) mBundle.get(Support.EXTRA_METHOD), (String) mBundle.get(Support.EXTRA_URL), requestParams,
                    (HashMap) mBundle.get(Support.EXTRA_HEADER), (String) mBundle.get(Support.EXTRA_USERNAME), (String) mBundle.get(Support.EXTRA_PASSWORD));
        }

        protected void onPostExecute(Boolean result) {
            HashMap<String, String> header = (HashMap<String, String>) mBundle.get(Support.EXTRA_HEADER);
			if (success == false) {
				// IF FAIL IN THE MIDDLE REQUEST NOTHING WILL NOT SAVE, IF YOU WANT SAVE NO COMPLETED RESPONSE DATA CALL mAlertList/mEventList
				sendBackHeadler();
			} else  if (mHandler.onError() == null && header.get(Support.HEADER_X_CREATED) == null) {
                int compateHeaderAndBody = 0;
                if (mAlertList != null) {
                    mAlertList.addAll(Support.parseByteArrayAlerts(mHandler.onResponseBody()));
                    compateHeaderAndBody = Integer.valueOf(mAlertList.size()).compareTo(Integer.valueOf(mHandler.onHeader()));
                } else if (mEventList != null) {
                    mEventList.addAll(Support.parseByteArrayEvents(mHandler.onResponseBody()));
                    compateHeaderAndBody = Integer.valueOf(mEventList.size()).compareTo(Integer.valueOf(mHandler.onHeader()));
                }
                if (compateHeaderAndBody < 0) {
                    Integer offset = Integer.valueOf(header.get(Support.HEADER_X_OFFSET)) + Integer.valueOf(header.get(Support.HEADER_X_LIMIT));
                    header.put(Support.HEADER_X_OFFSET, offset.toString());
                    mBundle.putSerializable(Support.EXTRA_HEADER, header);
                    if (mAlertList != null) {
                        new MenuAsyncTask(mAlertList, mBundle).execute();
                    } else {
                        new MenuAsyncTask(mEventList, mBundle, null).execute();
                    }
                } else if (compateHeaderAndBody == 0 || compateHeaderAndBody > 0) {
                    sendBackHeadler();
                }
            } else {
                if ((header.get(Support.HEADER_X_CREATED) != null) && (mHandler.onError() == null)) {
                    if (mAlertList != null) {
                        mAlertList.addAll(Support.parseByteArrayAlerts(mHandler.onResponseBody()));
                    } else {
                        mEventList.addAll(Support.parseByteArrayEvents(mHandler.onResponseBody()));
                    }
                }
                sendBackHeadler();
            }
        }

        public void sendBackHeadler() {
            try {
                Message message = Message.obtain();
                Bundle bundle = new Bundle();
				if (success == false) {
					mIntegooRestClient.cancel(getApplicationContext());
					bundle.putString(Support.EXTRA_RESPONSE_ERROR, getApplicationContext().getString(R.string.alert_messsage_no_internet_connection));
					message.setData(bundle);
					mMessanger.send(message);
					return;
				} else if (mHandler.onError() == null) {
                    bundle.putString(Support.EXTRA_RESPONSE_HEADER, mHandler.onHeader());
                    if (mAlertList != null) {
                        bundle.putSerializable(Support.EXTRA_RESPONSE_BODY, mAlertList);     //TODO CREATE PARCELABLE AND CHANGE LIST TO SET
                    } else {
                        bundle.putSerializable(Support.EXTRA_RESPONSE_BODY, mEventList);     //TODO CREATE PARCELABLE AND CHANGE LIST TO SET
                    }
                }
                bundle.putString(Support.EXTRA_RESPONSE_ERROR, mHandler.onError());
                message.setData(bundle);
                mMessanger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public boolean createHttpConnection(String method, String url, RequestParams requestParams, HashMap<String, String> header, String username, String password) {
			final int CHECK_MILISECONDS = 250;
			int numberIterate = 0;
			success = false;
            mIntegooRestClient = new IntegooRestClient(username, password);
            if (method.equals(Support.GET)) {
				mIntegooRestClient.get(getApplicationContext(), url, requestParams, header, mHandler);
            }
            if (method.equals(Support.POST)) {
				mIntegooRestClient.post(getApplicationContext(), url, header, mHandler);
            }
			while (true) {
				if(success) {
					break;
				}
				numberIterate = CHECK_MILISECONDS + numberIterate;
				if (numberIterate >= Support.CONNECTION_TIMEOUT) {
					Log.i(MenuAsyncTask.class.getName(), " number iterate expire");
					break;
				}
				try {
					Thread.sleep(CHECK_MILISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
            return true;
        }
    }

    //HANDLER HTTP CONNECTION
    private class AlertHandler extends AsyncHttpResponseHandler implements ListenerHttpResponseHandler {

        private Integer mStatusCode;
        private Header[] mHeaders;
        private byte[] mResponseBody;
        private Throwable mError;

        private AlertHandler() {
        }

        @Override
        public void onStart() {
            super.onStart();
            Log.d(AlertHandler.class.getName(), " onStart ");
            // Initiated the request
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            super.onSuccess(statusCode, headers, responseBody);
            Log.i(TAG, " onSuccess - statusCode" + statusCode + " headers" +headers.toString());
            mStatusCode = statusCode;
            mHeaders = headers;
            mResponseBody = responseBody;
        }

		@Override
		public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
			super.onFailure(statusCode, headers, responseBody, error);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
		}

		@Override
		public void onFailure(int statusCode, Throwable error, String content) {
			super.onFailure(statusCode, error, content);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
		}

        @Override
        public void onRetry() {
            super.onRetry();
            Log.i(TAG, " onRetry ");
            // Request was retried
        }

        @Override
        public void onProgress(int bytesWritten, int totalSize) {
            super.onProgress(bytesWritten, totalSize);
            Log.i(TAG, " onProgress ");
            // Progress notification
        }

        @Override
        public void onFinish() {
            super.onFinish();
			success = true;
			Log.i(TAG, " onFinish ");
            // Completed the request (either success or failure)
        }

        @Override
        public Integer onStatusCode() {
            Log.i(TAG, "status code:" + mStatusCode);
            return mStatusCode;
        }

        @Override
        public String onHeader() {
            Log.i(TAG, "onHeader");
            String count = null;
            Header[] headers = mHeaders;
            for (Header header : headers) {
                Log.i(TAG, "header response key:"+ header.getName() +" value:" + header.getValue());
                if (header.getName().equals(Support.HEADER_X_COUNT)) {
                    count = header.getValue();
                }
            }
            return count;
        }

        @Override
        public byte[] onResponseBody() {
            Log.i(TAG, "body:"+ new String(mResponseBody));
            return mResponseBody;
        }

        @Override
        public String onError() {
            Log.i(TAG, "errorMessage:"+ mError);
            if (mError == null) {
                return null;
            }
            return mError.toString();
        }
    }
}
