package cz.integoo.integra.thesis_android.thesis.support.parceleable;

import android.os.Parcel;
import android.os.Parcelable;

import com.loopj.android.http.RequestParams;

import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;

/**
 * Created by horkavlna on 24/08/14.
 */
public class UrlRequestParam implements Parcelable {

    private String fields;
    private String alertStatus;

    public UrlRequestParam(String fields, AlertStatus alertStatus) {
        this.fields = fields;
        this.alertStatus = alertStatus.getStatus();
    }

    public UrlRequestParam(String fields) {
        this.fields = fields;
    }

    public UrlRequestParam(Parcel in) {
        readFromParcel(in);
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public String getAlertStatus() {
        return alertStatus;
    }

    public void setAlertStatus(String alertStatus) {
        this.alertStatus = alertStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fields);
        dest.writeString(alertStatus);
    }

    private void readFromParcel(Parcel in) {
        fields = in.readString();
        alertStatus = in.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public UrlRequestParam createFromParcel(Parcel in) {
            return new UrlRequestParam(in);
        }

        public UrlRequestParam[] newArray(int size) {
            return new UrlRequestParam[size];
        }
    };
}