package cz.integoo.integra.thesis_android.thesis.support;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;

/**`
 * Created by horkavlna on 24/08/14.
 */

public class AlertStatusDeserializer implements JsonDeserializer<AlertStatus> {

    @Override
    public AlertStatus deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        String status = element.getAsString();
        return AlertStatus.fromKey(status);
    }

}
