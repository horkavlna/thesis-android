package cz.integoo.integra.thesis_android.thesis.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.base.GeneralActivity;
import cz.integoo.integra.thesis_android.thesis.adapter.FilterCursorAdapter;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;
import cz.integoo.integra.thesis_android.thesis.model.Filter;
import cz.integoo.integra.thesis_android.thesis.model.ImageIcon;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.persistence.ApplicationDatabase;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import cz.integoo.integra.thesis_android.thesis.service.FilterService;
import cz.integoo.integra.thesis_android.thesis.service.IconService;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.ToastMessage;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by horkavlna on 05/04/14.
 */
public class FilterActivity extends GeneralActivity implements OnRefreshListener, AdapterView.OnItemClickListener {

    private static final String TAG = FilterActivity.class.getName();

    private ListView mListview;
    private ActionBar mActionBar;
    private FilterCursorAdapter mFilterCursorAdapter;
    private PullToRefreshLayout mPullToRefreshLayoutFilter;
    private PersistentData mPersistentData;
    private ApplicationDatabase mDatabase;
    private User mUser;
	private TextView mEmptyList;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mPersistentData = new PersistentData(getApplicationContext());
        mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
        setContentView(R.layout.activity_filter);
        setProgressDialog();
        mListview = (ListView) findViewById(android.R.id.list);
        mListview.setOnItemClickListener(this);
        mListview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mEmptyList = (TextView) findViewById(R.id.emptyList);
		reloadList();
        setActionBar();
    }

    public void setActionBar() {
        mActionBar = getActionBar();
		mActionBar.setCustomView(R.layout.actionbar_custom);
		TextView textView = (TextView) mActionBar.getCustomView().findViewById(R.id.title_actionbar);
		textView.setText(getApplicationContext().getString(R.string.filter_title));
		textView.setTextSize(25);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		mActionBar.setLogo(new ColorDrawable(Color.TRANSPARENT));
		mActionBar.setIcon(new ColorDrawable(Color.TRANSPARENT));
	}

    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        if (mPersistentData.getSignedUserName() != null) {
            mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
            reloadList();
        }
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        ApplicationDatabase.closeManaged();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    public void fetchUser() {
        mUser = mDatabase.getUser(mPersistentData.getSignedUserName());
    }

    public void reloadList() {
        if (mUser == null) {
            fetchUser();
        }
        Cursor databaseCursor = mDatabase.getCursorFilter(Filter.class, mPersistentData.getSignedUserName());
		if (databaseCursor.getCount() != 0) {
			mEmptyList.setVisibility(View.GONE);
			if  (mUser.getSelectedFilter()!=null) {
				Log.i(TAG, "showfilterbeforeselected:" + mUser.getSelectedFilter().toString());
			}
			mFilterCursorAdapter = new FilterCursorAdapter(this, databaseCursor, mUser.getSelectedFilter()!=null?mUser.getSelectedFilter().getIdfilter():0);
			mListview.setAdapter(mFilterCursorAdapter);
		} else {
			mEmptyList.setVisibility(View.VISIBLE);
			mListview.setAdapter(null);
		}
    }

    public void setProgressDialog() {
        mPullToRefreshLayoutFilter = ((PullToRefreshLayout) findViewById(R.id.ptr_layout));
        ActionBarPullToRefresh.from(this)
                // Mark All Children as pullable
                .allChildrenArePullable()
                        // Set a OnRefreshListener
                .listener(this)
                        // Finally commit the setup to our PullToRefreshLayout
                .setup(mPullToRefreshLayoutFilter);
    }

    @Override
    public void onRefreshStarted(View view) {
        Log.i(TAG, "onRefreshStarted");
        //DOWNLOAD DATA FROM SERVER
        if (Support.checkNetwork(this)) {
            if (mDatabase.isOpen()) {
                User user = mDatabase.getUser(mPersistentData.getSignedUserName());

                Intent intentFilter = new Intent(getApplicationContext(), FilterService.class);
                intentFilter.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerFilter));
                intentFilter.putExtra(Support.EXTRA_METHOD, Support.GET);
                intentFilter.putExtra(Support.EXTRA_URL, mPersistentData.getServerAddress() + Support.URL_FILTER);
                intentFilter.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createFilterRequestParam()));
                //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                intentFilter.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                intentFilter.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                intentFilter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startService(intentFilter);

				Log.i(TAG, "download image icon");
				Intent intentImageIcon = new Intent(getApplicationContext(), IconService.class);
				intentImageIcon.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerImageIcon));
				intentImageIcon.putExtra(Support.EXTRA_METHOD, Support.GET);
				intentImageIcon.putExtra(Support.EXTRA_URL, mPersistentData.getServerAddress() + Support.URL_IMAGE_ICON);
                intentFilter.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createIconRequestParam()));
                //TODO CREATE PARCELABLE AND SEND STRINGENTITY
				intentImageIcon.putExtra(Support.EXTRA_USERNAME, user.getUsername());
				intentImageIcon.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
				intentImageIcon.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startService(intentImageIcon);
                mPullToRefreshLayoutFilter.setEnabled(false);
            }
        } else {
            showErrorMessage(this.getString(R.string.alert_messsage_no_internet_connection));
        }
    }

    private Handler handlerFilter = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
                mPullToRefreshLayoutFilter.setEnabled(true);
                setProgressDialog();    //beacuse this is bug in progress dialog in first refresh is ok, second is not valid and third is ok
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage != null) {
				showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
            } else {
                mPullToRefreshLayoutFilter.setRefreshing(false);
                mPullToRefreshLayoutFilter.setEnabled(true);
                //SAVE TO THE DATABASE
                User user = mDatabase.getUser(mPersistentData.getSignedUserName());
                ArrayList<Filter> myModelList = Support.parseByteToFilter(bundle.getByteArray(Support.EXTRA_RESPONSE_BODY));
				for (Filter filter : myModelList) {
					Log.i(TAG, "get filter list" + filter.toString());
				}
                mDatabase.saveFilter(myModelList, user);
                reloadList();
                Log.i(TAG, "save to database filter:" + myModelList.size());
            }
        }
    };

    private Handler handlerImageIcon = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handlerImageIcon");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage == null) {
                //SAVE TO THE DATABASE
                ArrayList<ImageIcon> myModelList = Support.parseByteArrayImageIcon(bundle.getByteArray(Support.EXTRA_RESPONSE_BODY));
                mDatabase.saveImageIcon(myModelList, mPersistentData.getSignedUserName());
                Log.i(TAG, "save to database image icon:" + myModelList.size());
            }
        }
    };

    public void showErrorMessage(String message) {
        mPullToRefreshLayoutFilter.setRefreshing(false);
        mPullToRefreshLayoutFilter.setEnabled(false);
		new ToastMessage(this, message, handlerFilter, R.id.activity_filter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		removeAllChecks(mListview);
		mFilterCursorAdapter.setCheckFilter(view, mFilterCursorAdapter.getCursor());
        long inndexFilter = mFilterCursorAdapter.getCursor().getLong(mFilterCursorAdapter.getCursor().getColumnIndex(Filter.COLUMN_ID_FILTER));
        Log.i(TAG, "setOnItemClickListener onListItemClick indexRow:" + inndexFilter + " item:" + position);

        Filter selectedFilter = mDatabase.getFilterId(inndexFilter);
		mUser.setSelectedFilter(selectedFilter);
		mDatabase.saveUser(mUser);
		finish();
    }

    private void removeAllChecks(ViewGroup vg) {
        View v = null;
        for(int i = 0; i < vg.getChildCount(); i++){
            try {
                v = vg.getChildAt(i);
                ((RadioButton)v).setChecked(false);
            }
            catch(Exception e1){ //if not checkBox, null View, etc
                try {
                    removeAllChecks((ViewGroup)v);
                }
                catch(Exception e2){ //v is not a view group
                    continue;
                }
            }
        }
    }
}
