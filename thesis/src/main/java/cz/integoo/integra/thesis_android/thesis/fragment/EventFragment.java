package cz.integoo.integra.thesis_android.thesis.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.MenuAcvitity;
import cz.integoo.integra.thesis_android.thesis.adapter.EventCursorAdapter;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;
import cz.integoo.integra.thesis_android.thesis.model.Event;
import cz.integoo.integra.thesis_android.thesis.model.Filter;
import cz.integoo.integra.thesis_android.thesis.model.ImageIcon;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.service.MenuService;
import cz.integoo.integra.thesis_android.thesis.support.DetailListViewListener;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.ToastMessage;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by horkavlna on 30/03/14.
 */
public class EventFragment extends ListFragment implements OnRefreshListener {

    private static final String TAG = EventFragment.class.getName();

    private MenuAcvitity mMenuActivity;
    private View mFragmentView;
    private ListView mListview;
    private EventCursorAdapter mEventCursorAdapter;
    private DetailListViewListener mListener;
    private Filter mFilter;
    private User mUser;
	private TextView mEmptyList;
	private static EventFragment eventFragment;

    @Override
    public void onAttach(Activity activity) {
        Log.i(TAG, "onAttach");
        super.onAttach(activity);
        mListener = (DetailListViewListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mMenuActivity = (MenuAcvitity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        mFragmentView = inflater.inflate(R.layout.fragment_event_list, container, false);
        setFilterName();
        if (mMenuActivity.getmPullToRefreshLayoutEvent() == null) {
            Log.i(TAG, "setMenuVisibility mPullToRefreshLayout");
            setProgressDialog();
        }
		mEmptyList = (TextView) mFragmentView.findViewById(R.id.emptyList);
		mListview = (ListView) mFragmentView.findViewById(android.R.id.list);
        return mFragmentView;
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        setInstanceBroadcast(this);
        if (mMenuActivity.getmPersistentData().getSignedUserName() != null) {
            fetchUser();
            if(mUser.getSelectedFilter() != null) {
                mFilter = mMenuActivity.getmDatabase().getFilterId(mUser.getSelectedFilter().getIdfilter());
			}
			updateFilterName();
            reloadList();
            if (mEventCursorAdapter != null) {
                mEventCursorAdapter.runUpdateTime();
            }
        }
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        setInstanceBroadcast(null);
        if (mEventCursorAdapter != null) {
            mEventCursorAdapter.stopUpdateTime();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    public static EventFragment getInstanceBroadcast() {
        return EventFragment.eventFragment;
    }

    public static void setInstanceBroadcast(EventFragment eventFragment) {
        EventFragment.eventFragment = eventFragment;
    }

    public void fetchUser() {
        mUser = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());
	}

    public void updateFilterName() {
        TextView textFilter = (TextView) mFragmentView.findViewById(R.id.text_title_filter);
        if (mUser.getSelectedFilter() != null && mFilter != null) {
			textFilter.setText(mFilter.getName());
        } else {
            textFilter.setText(mMenuActivity.getString(R.string.filter_no_title));
        }
    }

    public void setFilterName() {
        LinearLayout layout_filter = (LinearLayout) mFragmentView.findViewById(R.id.layout_filter);
        layout_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFilterSelected();
            }
        });
    }

    public EventCursorAdapter getmEventCursorAdapter() {
        return mEventCursorAdapter;
    }

    public void reloadList() {
        if (mFilter != null) {
            Cursor databaseCursor = mMenuActivity.getmDatabase().getCursorEvent(Event.class, mFilter);
            Log.i(TAG, "reloadList idFilter " + mFilter.toString() + " with data number:" + databaseCursor.getCount());
            if (databaseCursor.getCount() != 0) {
                MatrixCursor extras = new MatrixCursor(new String[] { Event.COLUMN_ID_EVENT_INTEGOO, Event.COLUMN_MESSAGE, Event.COLUMN_USER,  Event.COLUMN_DEVICE,
                        Event.COLUMN_PLACE, Event.COLUMN_CREATED, Event.COLUMN_FILTER, Event.COLUMN_IMAGE_ICON_FOREIGN, Event.COLUMN_IMAGE,
                        ImageIcon.COLUMN_IMAGE});
                extras.addRow(new String[] { "-1", String.format("%s %d %s", mMenuActivity.getString(R.string.alert_next_fetch_first),
                        mMenuActivity.getmPersistentData().getFetchLimit(), mMenuActivity.getString(R.string.event_next_fetch_second))
                        , null, null, null, null, null, null, null, null});

                Cursor[] cursors = {databaseCursor, extras};
                Cursor extendedCursor = new MergeCursor(cursors);

				mEmptyList.setVisibility(View.GONE);
				mEventCursorAdapter = new EventCursorAdapter(mMenuActivity, extendedCursor, mMenuActivity.getmDatabase().getAllImageIcon());
                mListview.setAdapter(mEventCursorAdapter);
            } else {
				mEmptyList.setVisibility(View.VISIBLE);
				mListview.setAdapter(null);
            }
        }
    }

    public void setProgressDialog() {
        mMenuActivity.setmPullToRefreshLayoutEvent((PullToRefreshLayout) mFragmentView.findViewById(R.id.ptr_layout));
        ActionBarPullToRefresh.from(getActivity())
                // Mark All Children as pullable
                .allChildrenArePullable()
                        // Set a OnRefreshListener
                .listener(this)
                        // Finally commit the setup to our PullToRefreshLayout
                .setup(mMenuActivity.getmPullToRefreshLayoutEvent());
    }

    @Override
    public void onRefreshStarted(View view) {
        Log.i(TAG, "onRefreshStarted");
        //DOWNLOAD DATA FROM SERVER
        if (mMenuActivity != null && mFilter != null) {
            if (Support.checkNetwork(mMenuActivity)) {
                if (mMenuActivity.getmDatabase().isOpen()) {
                    User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());

                    Intent intentEvent = new Intent(mMenuActivity.getApplicationContext(), MenuService.class);
                    intentEvent.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerEvent));
                    intentEvent.putExtra(Support.EXTRA_METHOD, Support.GET);
                    intentEvent.putExtra(Support.EXTRA_URL, mMenuActivity.getmPersistentData().getServerAddress() + Support.URL_EVENTS);
                    intentEvent.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createEventRequestParam()));
                    intentEvent.putExtra(Support.EXTRA_HEADER,
                            Support.createAllEventHttpHeader(mMenuActivity.getmPersistentData().getFetchLimit().toString(), "0", String.valueOf(mFilter.getId())));
                    intentEvent.putExtra(Support.EXTRA_IS_ALERT, false);
                    //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                    intentEvent.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                    intentEvent.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                    intentEvent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mMenuActivity.startService(intentEvent);
					mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
                }
            } else {
                showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_no_internet_connection));
				mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(false);
				mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
            }
        } else {
			mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
            showErrorMessage(mMenuActivity.getString(R.string.filter_no_title));
        }
    }

    private HashMap<String, String> createNextHttpHeader(Integer offset, String from) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(Support.HEADER_X_LIMIT, mMenuActivity.getmPersistentData().getFetchLimit().toString());
        header.put(Support.HEADER_X_CREATED, from);
        header.put(Support.HEADER_X_OFFSET, offset.toString());
		header.put(Support.HEADER_X_FILTER, String.valueOf(mFilter.getId()));
        return header;
    }

    private Handler handlerEvent = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
				mMenuActivity.setAllItemPullToRefreshLayoutEnabled(true);
				setProgressDialog();    //beacuse this is bug in progress dialog in first refresh is ok, second is not valid and third is ok
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage != null) {
                showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
            } else {
				mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(false);
				mMenuActivity.setAllItemPullToRefreshLayoutEnabled(true);
                if (mFilter == null) {
                    mFilter = mMenuActivity.getmDatabase().getFilterId(mUser.getSelectedFilter().getIdfilter());
                }
                //SAVE TO THE DATABASE
                ArrayList<Event> myModelList = (ArrayList<Event>) bundle.getSerializable(Support.EXTRA_RESPONSE_BODY);
                mMenuActivity.getmDatabase().saveEvent(myModelList, mFilter);
                reloadList();
                Log.i(TAG, "save to database with filter:" + mFilter.getId());
            }
        }
    };

    public void showErrorMessage(String message) {
		mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(false);
		new ToastMessage(mMenuActivity, message, handlerEvent, R.id.menu);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String inndexRow = mEventCursorAdapter.getCursor().getString(mEventCursorAdapter.getCursor().getColumnIndex(Event.COLUMN_ID_EVENT_INTEGOO));
        Log.i(TAG, "setOnItemClickListener onListItemClick indexRow:" + inndexRow);

        if (inndexRow == null) {
            //fetch x limit
            int positionColumn = ((MergeCursor) mEventCursorAdapter.getItem(position - 1)).getColumnIndex(Event.COLUMN_CREATED);
            String createMinEvent =((MergeCursor) mEventCursorAdapter.getItem(position - 1)).getString(positionColumn);
            Log.i(TAG, "setOnItemClickListener onListItemClick last created id:" + createMinEvent);
            if (createMinEvent != null) {
                if (mMenuActivity != null) {
                    if (Support.checkNetwork(mMenuActivity)) {
                        if (mMenuActivity.getmDatabase().isOpen()) {
                            User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());
                            Intent intentEvent = new Intent(mMenuActivity.getApplicationContext(), MenuService.class);
                            intentEvent.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerEvent));
                            intentEvent.putExtra(Support.EXTRA_METHOD, Support.GET);
                            intentEvent.putExtra(Support.EXTRA_URL, mMenuActivity.getmPersistentData().getServerAddress() + Support.URL_EVENTS);
                            intentEvent.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createEventRequestParam()));
                            intentEvent.putExtra(Support.EXTRA_HEADER, createNextHttpHeader(0, createMinEvent));
                            intentEvent.putExtra(Support.EXTRA_IS_ALERT, false);
                            //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                            intentEvent.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                            intentEvent.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                            intentEvent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mMenuActivity.startService(intentEvent);
							mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(true);
							mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
                        }
                    } else {
                        showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_no_internet_connection));
						mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
                    }
                }
            }
        } else {
            //show detail event
            mListener.onEventDetailSelected(inndexRow);
        }
    }
}
