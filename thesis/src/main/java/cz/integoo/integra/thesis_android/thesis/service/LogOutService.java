package cz.integoo.integra.thesis_android.thesis.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.support.Support;


/**
 * Created by horkavlna on 27/03/14.
 */
public class LogOutService extends IntentService {

    private static final String TAG = LogOutService.class.getName();
    private boolean success;

    public LogOutService() {
        super("logout service");
    }

    public LogOutService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, " onHandleIntent ");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            new LogOutAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bundle);;
        }
    }

    @Override
    public boolean stopService(Intent name) {
        Log.i(TAG, " stopService ");
        return super.stopService(name);
    }

    //ASYNCTASK FOR CONNECTION
    private class LogOutAsyncTask extends AsyncTask<Bundle, Void, Boolean> {
        private Messenger mMessanger;
        private LogOutHandler mHandler;
		private IntegooRestClient mIntegooRestClient;

		@Override
        protected Boolean doInBackground(Bundle... bundle) {
            Log.i(LogOutAsyncTask.class.getName(), " doInBackground ");
            this.mMessanger = (Messenger) bundle[0].get(Support.EXTRA_HANDLER);
            this.mHandler = new LogOutHandler();
            return createHttpConnection((String) bundle[0].get(Support.EXTRA_URL), (String) bundle[0].get(Support.EXTRA_USERNAME), (String) bundle[0].get(Support.EXTRA_PASSWORD));
        }

        protected void onPostExecute(Boolean result) {
            Log.i(LogOutAsyncTask.class.getName(), " onPostExecute ");
            if (mHandler != null) {
                try {
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
					if (success == false) {
						mIntegooRestClient.cancel(getApplicationContext());
						bundle.putString(Support.EXTRA_RESPONSE_ERROR, getApplicationContext().getString(R.string.alert_messsage_no_internet_connection));
					} else {
						bundle.putString(Support.EXTRA_RESPONSE_ERROR, mHandler.onError());
					}
					message.setData(bundle);
					mMessanger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        public boolean createHttpConnection(String url, String username, String password) {
			final int CHECK_MILISECONDS = 250;
			int numberIterate = 0;
			success = false;
			mIntegooRestClient = new IntegooRestClient(username, password);
			mIntegooRestClient.delete(getApplicationContext(), url, this.mHandler);
			while (true) {
				if(success) {
					break;
				}
				numberIterate = CHECK_MILISECONDS + numberIterate;
				if (numberIterate >= Support.CONNECTION_TIMEOUT) {
					Log.i(LogOutAsyncTask.class.getName(), " number iterate expire");
					break;
				}
				try {
					Thread.sleep(CHECK_MILISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
            return true;
        }
    }

    //HANDLER HTTP CONNECTION
    private class LogOutHandler extends AsyncHttpResponseHandler implements ListenerHttpResponseHandler {

        private Integer mStatusCode;
        private Header[] mHeaders;
        private byte[] mResponseBody;
        private Throwable mError;

        private LogOutHandler() {
        }

        @Override
        public void onStart() {
            super.onStart();
            Log.i(LogOutHandler.class.getName(), " onStart");
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            super.onSuccess(statusCode, headers, responseBody);
            Log.i(LogOutHandler.class.getName(), " onSuccess - statusCode" + statusCode + " headers" +headers.toString());
            mStatusCode = statusCode;
            mHeaders = headers;
            mResponseBody = responseBody;
        }

		@Override
		public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
			super.onFailure(statusCode, headers, responseBody, error);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
		}

		@Override
		public void onFailure(int statusCode, Throwable error, String content) {
			super.onFailure(statusCode, error, content);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
		}

        @Override
        public void onRetry() {
            super.onRetry();
            Log.i(LogOutHandler.class.getName(), " onRetry ");
            // Request was retried
        }

        @Override
        public void onProgress(int bytesWritten, int totalSize) {
            super.onProgress(bytesWritten, totalSize);
            Log.d(LogOutHandler.class.getName(), " onProgress ");
            // Progress notification
        }

        @Override
        public void onFinish() {
            super.onFinish();
			success = true;
			Log.d(LogOutHandler.class.getName(), " onFinish ");
            // Completed the request (either success or failure)
        }

        @Override
        public Integer onStatusCode() {
            Log.i(LogOutHandler.class.toString(), "status code:" + mStatusCode);
            return mStatusCode;
        }

        @Override
        public String onHeader() {
            String userLocation = null;
//            HashMap<String, String> headerMap = new HashMap<String, String>();
            Header[] headers = mHeaders;
            for (Header header : headers) {
                Log.i(SigninService.class.toString(), "header value:"+ header.getName() +" value:" + header.getValue());
                if (header.getName().equals(Support.HEADER_LOCATION)) {
                    userLocation = header.getValue();
                }
//                headerMap.put(header.getName(), header.getValue());
            }
            return userLocation;
        }

        @Override
        public byte[] onResponseBody() {
            Log.i(LogOutHandler.class.toString(), "body:"+ new String(mResponseBody));
            return mResponseBody;
        }

        @Override
        public String onError() {
            Log.i(LogOutHandler.class.toString(), "errorMessage:"+ mError);
            if (mError == null) {
                return null;
            }
            return mError.toString();
        }
    }
}
