package cz.integoo.integra.thesis_android.thesis.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Arrays;

/**
 * Created by horkavlna on 29/03/14.
 */
@DatabaseTable
public class Alert {

	public static final String COLUMN_ID_ALERT = "_id";
	public static final String COLUMN_ID_ALERT_INTEGOO = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DEVICE_NAME = "device_name";
    public static final String COLUMN_PLACE_NAME = "place_name";
    public static final String COLUMN_CREATED_AT = "created_at";
    public static final String COLUMN_UPDATED_AT = "updated_at";
    public static final String COLUMN_TYPE_COLOR = "type_color";
    public static final String COLUMN_INSTRUCTION = "instruction";
    public static final String COLUMN_INSTRUCTION_DEF = "instruction_def";
    public static final String COLUMN_STATUS = "status";

    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_USER = "user";

	@DatabaseField(generatedId = true, columnName = COLUMN_ID_ALERT)
	private long idAlert;
    @SerializedName(COLUMN_ID_ALERT_INTEGOO)
    @DatabaseField(columnName = COLUMN_ID_ALERT_INTEGOO)
    private long id;
    @SerializedName(COLUMN_NAME)
    @DatabaseField(columnName = COLUMN_NAME)
    private String name;
    @SerializedName(COLUMN_DEVICE_NAME)
    @DatabaseField(columnName = COLUMN_DEVICE_NAME)
    private String deviceName;
    @SerializedName(COLUMN_PLACE_NAME)
    @DatabaseField(columnName = COLUMN_PLACE_NAME)
    private String placeName;
    @SerializedName(COLUMN_CREATED_AT)
    @DatabaseField(columnName = COLUMN_CREATED_AT)
    private String createdAt;
    @SerializedName(COLUMN_UPDATED_AT)
    @DatabaseField(columnName = COLUMN_UPDATED_AT)
    private String updatedAt;
    @SerializedName(COLUMN_TYPE_COLOR)
    @DatabaseField(columnName = COLUMN_TYPE_COLOR)
    private String typeColor;
    @SerializedName(COLUMN_INSTRUCTION)
    @DatabaseField(columnName = COLUMN_INSTRUCTION)
    private String instruction;
    @SerializedName(COLUMN_INSTRUCTION_DEF)
    @DatabaseField(columnName = COLUMN_INSTRUCTION_DEF)
    private String instructionDef;
    @SerializedName(COLUMN_STATUS)
    @DatabaseField(columnName = COLUMN_STATUS, dataType = DataType.ENUM_STRING)
    private AlertStatus status;
    @SerializedName(COLUMN_IMAGE)
    @DatabaseField(columnName = COLUMN_IMAGE, dataType = DataType.BYTE_ARRAY)
    private byte[] image;
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USER)
    private User user;

    public long getIdAlert() {
        return idAlert;
    }

    public void setIdAlert(long idAlert) {
        this.idAlert = idAlert;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTypeColor() {
        return typeColor;
    }

    public void setTypeColor(String typeColor) {
        this.typeColor = typeColor;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getInstructionDef() {
        return instructionDef;
    }

    public void setInstructionDef(String instructionDef) {
        this.instructionDef = instructionDef;
    }

    public AlertStatus getStatus() {
        return status;
    }

    public void setStatus(AlertStatus status) {
        this.status = status;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Alert)) {
            return false;
        }
        Alert c = (Alert) o;
        return c.getId() == getId();
    }

    @Override
    public String toString() {
        return "Alert{" +
                "idAlert=" + idAlert +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", placeName='" + placeName + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", typeColor='" + typeColor + '\'' +
                ", instruction='" + instruction + '\'' +
                ", instructionDef='" + instructionDef + '\'' +
                ", status=" + status +
                ", image=" + Arrays.toString(image) +
                ", user=" + user +
                '}';
    }
}
