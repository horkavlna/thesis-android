package cz.integoo.integra.thesis_android.thesis.support.parceleable;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.MenuItem;

/**
 * Created by horkavlna on 13/05/14.
 */
public class Rotation implements Parcelable {

	private MenuItem menuItem;

	public Rotation(Parcel in) {
		readFromParcel(in);
	}

	public Rotation() {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(menuItem);
	}

	private void readFromParcel(Parcel in) {
		menuItem = (MenuItem) in.readValue(MenuItem.class.getClassLoader());
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Rotation createFromParcel(Parcel in) {
			return new Rotation(in);
		}
		public Rotation[] newArray(int size) {
			return new Rotation[size]; }
	};
}
