package cz.integoo.integra.thesis_android.thesis.activity;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.base.GeneralFragmentActivity;
import cz.integoo.integra.thesis_android.thesis.fragment.TabsPagerAdapter;
import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.persistence.ApplicationDatabase;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import cz.integoo.integra.thesis_android.thesis.support.DetailListViewListener;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;

/**
 * Created by horkavlna on 30/03/14.
 */
public class MenuAcvitity extends GeneralFragmentActivity implements ActionBar.TabListener, DetailListViewListener {

    private static final String TAG = MenuAcvitity.class.getName();

    private MenuAcvitity context;
    private Menu mMenu;
    private Intent mIntent;
    private PersistentData mPersistentData;
    private ApplicationDatabase mDatabase;

    private ViewPager mViewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar mActionBar;

	private static PullToRefreshLayout mPullToRefreshLayoutNewAlert;
    private static PullToRefreshLayout mPullToRefreshLayoutExistAlert;
    private static PullToRefreshLayout mPullToRefreshLayoutEvent;

    private Long mIdAlertRemotePushNotification;
    private ViewGroup mContainer;

    private static final Integer[] tabs = {R.string.action_bar_tabs_new_alert, R.string.action_bar_tabs_exist_alert,
            R.string.action_bar_tabs_event};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        mPersistentData = new PersistentData(getApplicationContext());
        mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
        context = this;
        Log.i(TAG, "USER:"+ mDatabase.getUser(mPersistentData.getSignedUserName()));
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        mIdAlertRemotePushNotification = 0l;
        mDatabase = ApplicationDatabase.createManaged(getApplicationContext());
        if(mIntent != null) {
            Bundle bundle = mIntent.getExtras();
            if (bundle != null && bundle.containsKey(DetailNewAlert.ID_ALERT)) {
                Long idAlert = bundle.getLong(DetailNewAlert.ID_ALERT);
                Log.i(TAG, "idAlert from remote push notification" + idAlert);
                Alert alert = mDatabase.getAlert(idAlert);
                if (alert == null) {
                    Log.i(TAG, "download alerts");
                    mIdAlertRemotePushNotification = idAlert;
                } else {
                    Log.i(TAG, "alert exist");
                    showDetailNewAlert(idAlert);
                }
            }
        }
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
        ApplicationDatabase.closeManaged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar, menu);
        menu.findItem(R.id.action_done).setVisible(false);

        mActionBar = getActionBar();
		mActionBar.setCustomView(R.layout.actionbar_custom);
		TextView textView = (TextView) mActionBar.getCustomView().findViewById(R.id.title_actionbar);
		textView.setText(mPersistentData.getSignedUserName());
		textView.setTextSize(25);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM  | ActionBar.DISPLAY_SHOW_HOME);
		mActionBar.setLogo(new ColorDrawable(Color.TRANSPARENT));
		mActionBar.setIcon(new ColorDrawable(Color.TRANSPARENT));
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		mActionBar.setDisplayUseLogoEnabled(false);

		mViewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOnPageChangeListener(new Swipping());
        mViewPager.setOffscreenPageLimit(2);
        addTabs();

		setAllItemPullToRefreshLayoutEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                settingsPressed(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onNewAlertDetailSelected(long idAlert) {
        Log.i(TAG, "onNewAlertDetailSelected");
        showDetailNewAlert(idAlert);
    }

    public void showDetailNewAlert(long idAlert) {
        Intent intent = new Intent();
        intent.setClassName(getApplicationContext(), DetailNewAlert.class.getName());
        intent.putExtra(DetailNewAlert.ID_ALERT, idAlert);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onExistAlertDetailSelected(long idAlert) {
        Log.i(TAG, "onExistAlertDetailSelected");
        Intent intent = new Intent();
        intent.setClassName(getApplicationContext(), DetailExistAlert.class.getName());
        intent.putExtra(DetailExistAlert.ID_ALERT, idAlert);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onEventDetailSelected(String idEvent) {
        Log.i(TAG, "onEventDetailSelected");
        Intent intent = new Intent();
        intent.setClassName(getApplicationContext(), DetailEvent.class.getName());
        intent.putExtra(DetailEvent.ID_EVENT, idEvent);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onFilterSelected() {
        Log.i(TAG, "onFilterSelected");
        Intent intent = new Intent();
        intent.setClassName(getApplicationContext(), FilterActivity.class.getName());
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {
        Log.i(TAG, "onTabSelected");
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction ft) {

    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
    }

    private class Swipping implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            Log.i(TAG, "onPageSelected");
            mActionBar.setSelectedNavigationItem(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.mIntent = intent;
    }

    private void addTabs() {
        for (Integer tabName : tabs) {
            mActionBar.addTab(mActionBar.newTab().setText(getString(tabName)).setTabListener(this));
        }
    }

    public void settingsPressed(MenuItem item) {
        item.setTitle(R.string.preference_settings_title);
        Intent intent = new Intent(MenuAcvitity.this, SettingsActivity.class);
        startActivity(intent);
    }

    public boolean checkServiceRunning(String servisClassName) {
        Log.i(TAG, "checkServiceRunning");
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (servisClassName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public Menu getmMenu() {
        return mMenu;
    }

    public PersistentData getmPersistentData() {
        return mPersistentData;
    }

    public ApplicationDatabase getmDatabase() {
        return mDatabase;
    }

	public PullToRefreshLayout getmPullToRefreshLayoutEvent() {
        return mPullToRefreshLayoutEvent;
    }

    public PullToRefreshLayout getmPullToRefreshLayoutNewAlert() {
        return mPullToRefreshLayoutNewAlert;
    }

    public PullToRefreshLayout getmPullToRefreshLayoutExistAlert() {
        return mPullToRefreshLayoutExistAlert;
    }

    public void setmPullToRefreshLayoutNewAlert(PullToRefreshLayout mPullToRefreshLayoutNewAlert) {
        this.mPullToRefreshLayoutNewAlert = mPullToRefreshLayoutNewAlert;
    }

    public void setmPullToRefreshLayoutExistAlert(PullToRefreshLayout mPullToRefreshLayoutExistAlert) {
        this.mPullToRefreshLayoutExistAlert = mPullToRefreshLayoutExistAlert;
    }

    public void setmPullToRefreshLayoutEvent(PullToRefreshLayout mPullToRefreshLayoutEvent) {
        this.mPullToRefreshLayoutEvent = mPullToRefreshLayoutEvent;
    }

    public Long getmIdAlertRemotePushNotification() {
        return mIdAlertRemotePushNotification;
    }

    public void setmIdAlertRemotePushNotification(Long mIdAlertRemotePushNotification) {
        this.mIdAlertRemotePushNotification = mIdAlertRemotePushNotification;
    }

	public void setAllItemPullToRefreshLayoutRefreshing(boolean value) {
		if (mPullToRefreshLayoutNewAlert != null) {
			mPullToRefreshLayoutNewAlert.setRefreshing(value);
		}
		if (mPullToRefreshLayoutExistAlert != null) {
			mPullToRefreshLayoutExistAlert.setRefreshing(value);
		}
		if (mPullToRefreshLayoutEvent != null) {
			mPullToRefreshLayoutEvent.setRefreshing(value);
		}
	}

	public void setAllItemPullToRefreshLayoutEnabled(boolean value) {
		if (mPullToRefreshLayoutNewAlert != null) {
			mPullToRefreshLayoutNewAlert.setEnabled(value);
		}
		if (mPullToRefreshLayoutExistAlert != null) {
			mPullToRefreshLayoutExistAlert.setEnabled(value);
		}
		if (mPullToRefreshLayoutEvent != null) {
			mPullToRefreshLayoutEvent.setEnabled(value);
		}
	}
}

