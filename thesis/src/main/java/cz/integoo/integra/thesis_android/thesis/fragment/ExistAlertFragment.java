package cz.integoo.integra.thesis_android.thesis.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.activity.MenuAcvitity;
import cz.integoo.integra.thesis_android.thesis.adapter.ExistAlertCursorAdapter;
import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.service.MenuService;
import cz.integoo.integra.thesis_android.thesis.support.DetailListViewListener;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.ToastMessage;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by horkavlna on 30/03/14.
 */
public class ExistAlertFragment extends ListFragment implements OnRefreshListener {

    private static final String TAG = ExistAlertFragment.class.getName();

    private MenuAcvitity mMenuActivity;
    private View mFragmentView;
    private ListView mListview;
    private ExistAlertCursorAdapter mExistAlertCursorAdapter;
    private DetailListViewListener mListener;
	private TextView mEmptyList;
	private static ExistAlertFragment existAlertFragment;

    @Override
    public void onAttach(Activity activity) {
        Log.i(TAG, "onAttach");
        super.onAttach(activity);
        mListener = (DetailListViewListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mMenuActivity = (MenuAcvitity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        mFragmentView = inflater.inflate(R.layout.fragment_exist_alert_list, container, false);
        if (mMenuActivity.getmPullToRefreshLayoutExistAlert() == null) {
            Log.i(TAG, "setMenuVisibility mPullToRefreshLayout");
            setProgressDialog();
        }
        mListview = (ListView) mFragmentView.findViewById(android.R.id.list);
		mEmptyList = (TextView) mFragmentView.findViewById(R.id.emptyList);
		reloadList();
        return mFragmentView;
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        setInstanceBroadcast(this);
        if (mMenuActivity.getmPersistentData().getSignedUserName() != null) {
            reloadList();
            if (mExistAlertCursorAdapter != null) {
                mExistAlertCursorAdapter.runUpdateTime();
            }
        }
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        setInstanceBroadcast(null);
        if (mExistAlertCursorAdapter != null) {
            mExistAlertCursorAdapter.stopUpdateTime();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    public static ExistAlertFragment getInstanceBroadcast() {
        return ExistAlertFragment.existAlertFragment;
    }

    public static void setInstanceBroadcast(ExistAlertFragment existAlertFragment) {
        ExistAlertFragment.existAlertFragment = existAlertFragment;
    }

    public void reloadList() {
        Cursor databaseCursor = mMenuActivity.getmDatabase().getCursorAlert(Alert.class, mMenuActivity.getmPersistentData().getSignedUserName(), AlertStatus.ACCEPTED);
        Log.i(TAG,"reloadList:" + mMenuActivity.getmPersistentData().getSignedUserName() + " getCount: " + databaseCursor.getCount());
        if (databaseCursor.getCount() != 0) {
            MatrixCursor extras = new MatrixCursor(new String[] { Alert.COLUMN_ID_ALERT_INTEGOO, Alert.COLUMN_NAME, Alert.COLUMN_DEVICE_NAME, Alert.COLUMN_PLACE_NAME, Alert.COLUMN_CREATED_AT,
                    Alert.COLUMN_UPDATED_AT, Alert.COLUMN_TYPE_COLOR, Alert.COLUMN_STATUS, Alert.COLUMN_INSTRUCTION, Alert.COLUMN_INSTRUCTION_DEF, Alert.COLUMN_IMAGE, Alert.COLUMN_USER});
            extras.addRow(new String[] { "-1", String.format("%s %d %s", mMenuActivity.getString(R.string.alert_next_fetch_first),
                    mMenuActivity.getmPersistentData().getFetchLimit(), mMenuActivity.getString(R.string.alert_next_fetch_second))
                    , null, null, null, null, null, null, null, null, null, null });

            Cursor[] cursors = {databaseCursor, extras};
            Cursor extendedCursor = new MergeCursor(cursors);
			mEmptyList.setVisibility(View.GONE);
            mExistAlertCursorAdapter = new ExistAlertCursorAdapter(mMenuActivity,extendedCursor);
            mListview.setAdapter(mExistAlertCursorAdapter);
        }  else {
			mEmptyList.setVisibility(View.VISIBLE);
			mListview.setAdapter(null);
        }
    }

    public void setProgressDialog() {
        mMenuActivity.setmPullToRefreshLayoutExistAlert((PullToRefreshLayout) mFragmentView.findViewById(R.id.ptr_layout));
        ActionBarPullToRefresh.from(getActivity())
                // Mark All Children as pullable
                .allChildrenArePullable()
                        // Set a OnRefreshListener
                .listener(this)
                        // Finally commit the setup to our PullToRefreshLayout
                .setup(mMenuActivity.getmPullToRefreshLayoutExistAlert());
    }

    @Override
    public void onRefreshStarted(View view) {
        Log.i(TAG, "onRefreshStarted");
        //DOWNLOAD DATA FROM SERVER
        if (mMenuActivity != null) {
            if (Support.checkNetwork(mMenuActivity)) {
                if (mMenuActivity.getmDatabase().isOpen()) {
                    User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());

                    Intent intentExistAlert = new Intent(mMenuActivity.getApplicationContext(), MenuService.class);
                    intentExistAlert.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerExistAlert));
                    intentExistAlert.putExtra(Support.EXTRA_METHOD, Support.GET);
                    intentExistAlert.putExtra(Support.EXTRA_URL, mMenuActivity.getmPersistentData().getServerAddress() + Support.URL_ALERTS);
                    intentExistAlert.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createAlarmRequestParam(), AlertStatus.ACCEPTED));
                    intentExistAlert.putExtra(Support.EXTRA_HEADER, Support.createAllAlertsHttpHeader(mMenuActivity.getmPersistentData().getFetchLimit().toString(),"0"));
                    intentExistAlert.putExtra(Support.EXTRA_IS_ALERT, true);
                    //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                    intentExistAlert.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                    intentExistAlert.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                    intentExistAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mMenuActivity.startService(intentExistAlert);
					mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
                }
            } else {
                showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_no_internet_connection));
            }
        }
    }

    private HashMap<String, String> createNextAlertsHttpHeader(Integer offset, String from) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(Support.HEADER_X_LIMIT, mMenuActivity.getmPersistentData().getFetchLimit().toString());
        header.put(Support.HEADER_X_CREATED, from);
        header.put(Support.HEADER_X_OFFSET, offset.toString());
        return header;
    }

    private Handler handlerExistAlert = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
				mMenuActivity.setAllItemPullToRefreshLayoutEnabled(true);
                setProgressDialog();    //beacuse this is bug in progress dialog in first refresh is ok, second is not valid and third is ok
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage != null) {
                showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
            } else {
				mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(false);
				mMenuActivity.setAllItemPullToRefreshLayoutEnabled(true);
                //SAVE TO THE DATABASE
                User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());
                ArrayList<Alert> myModelList = (ArrayList<Alert>) bundle.getSerializable(Support.EXTRA_RESPONSE_BODY);
                mMenuActivity.getmDatabase().saveAlerts(myModelList, user, AlertStatus.ACCEPTED);
                reloadList();
                Log.i(TAG, "save to database");
            }
        }
    };

    public void showErrorMessage(String message) {
		mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(false);
		mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
		new ToastMessage(mMenuActivity, message, handlerExistAlert, R.id.menu);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        long inndexRow = mExistAlertCursorAdapter.getCursor().getLong(mExistAlertCursorAdapter.getCursor().getColumnIndex(Alert.COLUMN_ID_ALERT_INTEGOO));
        Log.i(TAG, "setOnItemClickListener onListItemClick indexRow:" + inndexRow);

        if (inndexRow == -1) {
            //fetch x limit
            int positionColumn = ((MergeCursor) mExistAlertCursorAdapter.getItem(position - 1)).getColumnIndex(Alert.COLUMN_CREATED_AT);
            String createMinAlert =((MergeCursor) mExistAlertCursorAdapter.getItem(position - 1)).getString(positionColumn);
            Log.i(TAG, "setOnItemClickListener onListItemClick last created id:" + createMinAlert);
            if (createMinAlert != null) {
                if (mMenuActivity != null) {
                    if (Support.checkNetwork(mMenuActivity)) {
                        if (mMenuActivity.getmDatabase().isOpen()) {
                            User user = mMenuActivity.getmDatabase().getUser(mMenuActivity.getmPersistentData().getSignedUserName());
                            Intent intentExistAlert = new Intent(mMenuActivity.getApplicationContext(), MenuService.class);
                            intentExistAlert.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerExistAlert));
                            intentExistAlert.putExtra(Support.EXTRA_METHOD, Support.GET);
                            intentExistAlert.putExtra(Support.EXTRA_URL, mMenuActivity.getmPersistentData().getServerAddress() + Support.URL_ALERTS);
                            intentExistAlert.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createAlarmRequestParam(), AlertStatus.ACCEPTED));
                            intentExistAlert.putExtra(Support.EXTRA_HEADER, createNextAlertsHttpHeader(0, createMinAlert));
                            intentExistAlert.putExtra(Support.EXTRA_IS_ALERT, true);
                            //TODO CREATE PARCELABLE AND SEND STRINGENTITY
                            intentExistAlert.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                            intentExistAlert.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                            intentExistAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mMenuActivity.startService(intentExistAlert);
							mMenuActivity.setAllItemPullToRefreshLayoutRefreshing(true);
							mMenuActivity.setAllItemPullToRefreshLayoutEnabled(false);
                        }
                    } else {
                        showErrorMessage(mMenuActivity.getString(R.string.alert_messsage_no_internet_connection));
                    }
                }
            }
        } else {
            //show detail alert
            mListener.onExistAlertDetailSelected(inndexRow);
        }
    }
}
