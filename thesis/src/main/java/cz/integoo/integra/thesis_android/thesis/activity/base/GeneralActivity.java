package cz.integoo.integra.thesis_android.thesis.activity.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.TextView;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.integoo.integra.thesis_android.thesis.activity.SigninActivity;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.persistence.ApplicationDatabase;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import cz.integoo.integra.thesis_android.thesis.service.LogOutService;
import cz.integoo.integra.thesis_android.thesis.support.Support;

/**
 * Created by horkavlna on 05/04/14.
 */
public class GeneralActivity extends Activity {

    private static final String TAG = GeneralActivity.class.getName();

    private Timer mTimer;
    private TextView mDuration;
    private long mCreatedTime;
    private static boolean isAppWentToBackground = true;
    private static boolean isWindowFocused = false;
    private static boolean isBackPressed = false;

    // for handle application is on the background
    @Override
    protected void onStart() {
        Log.d(TAG, "onStart isAppWentToBackground " + isAppWentToBackground);
        applicationWillEnterForeground();
        super.onStart();
    }

    //App is in foreground
    private void applicationWillEnterForeground() {
        if (isAppWentToBackground && GeneralFragmentActivity.isIsAppWentToBackground() && GeneralPreferenceActivity.isIsAppWentToBackground()) {
            isAppWentToBackground = false;
            GeneralActivity.logOutUser(getApplicationContext());
          //  Toast.makeText(getApplicationContext(), "App is in foreground", Toast.LENGTH_SHORT).show();
        }
    }

	@Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop ");
        applicationdidenterbackground();
    }

    //App is Going to Background
    public void applicationdidenterbackground() {
        if (!isWindowFocused && !GeneralFragmentActivity.isIsWindowFocused() && !GeneralPreferenceActivity.isIsWindowFocused()) {
            isAppWentToBackground = true;
            GeneralFragmentActivity.setIsAppWentToBackground(true);
            GeneralPreferenceActivity.setIsAppWentToBackground(true);
            saveLastTimeInApplication(getApplicationContext());
        //    Toast.makeText(getApplicationContext(), "App is Going to Background", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (this instanceof GeneralActivity) {
        } else {
            isBackPressed = true;
        }
        Log.d(TAG, "onBackPressed " + isBackPressed + "" + this.getLocalClassName());
        super.onBackPressed();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        isWindowFocused = hasFocus;
        Log.i(TAG, "onWindowFocusChanged " + isWindowFocused);
        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public static boolean isIsBackPressed() {
        return isBackPressed;
    }

    public static boolean isIsAppWentToBackground() {
        return isAppWentToBackground;
    }

    public static boolean isIsWindowFocused() {
        return isWindowFocused;
    }

    public static void setIsAppWentToBackground(boolean isAppWentToBackground) {
        GeneralActivity.isAppWentToBackground = isAppWentToBackground;
    }

    public static void saveLastTimeInApplication(Context context) {
        PersistentData persistentData = new PersistentData(context);
        persistentData.putLastTimeInApplication(System.currentTimeMillis());
        persistentData.apply();
    }

    public static void logOutUser(Context context) {
        Log.i(TAG, "logOutUser" );
        PersistentData persistentData = new PersistentData(context);
        long logoutTime = persistentData.getTimeLogout();
        long lastTime = persistentData.getLastTimeInApplication();
        long currentTime = System.currentTimeMillis();
        if (lastTime != 0L && persistentData.getSignedUserName() !=null && logoutTime != 0L) {
            if (currentTime > (lastTime + logoutTime)) {
                //logout user
                Log.i(TAG, "user will be logout" );
                ApplicationDatabase database = ApplicationDatabase.createManaged(context);
                User user = database.getUser(persistentData.getSignedUserName());
                Intent intentLogOut = new Intent(context, LogOutService.class);
                intentLogOut.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerLogOutGeneral));
                intentLogOut.putExtra(Support.EXTRA_URL, persistentData.getServerAddress() + user.getLocation());
                intentLogOut.putExtra(Support.EXTRA_USERNAME, user.getUsername());
                intentLogOut.putExtra(Support.EXTRA_PASSWORD, user.getPassword());
                intentLogOut.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startService(intentLogOut);

                persistentData.clearPersistentData();
                persistentData.apply();
                Intent intent = new Intent();
                intent.setClassName(context, SigninActivity.class.getName());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else {
                Log.i(TAG, "short time for logout");
            }
        }
    }

    //TODO
    private static Handler handlerLogOutGeneral = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handlerLogOut");
            Bundle bundle = msg.getData();
            if (bundle.isEmpty()) {
                Log.i(TAG, "handleMessage from Toast");
                return;
            }
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage != null) {
                Log.e(TAG, "handleMessage error:" + errorMessage);
                //showErrorMessage(getResources().getString(R.string.alert_messsage_bad_network_request));
            } else {
                Log.i(TAG, "handleMessage ");
            }
        }
    };

    private class CountTimer extends TimerTask {

        public CountTimer() {

        }

        @Override
        public void run() {
            new CreatedTask((new Date().getTime()/1000))
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        }
    }

    private class CreatedTask extends AsyncTask<Void, Void, Void> {
        private Long mActuallTime;

        public CreatedTask(Long actuallTime) {
            mActuallTime = actuallTime;
        }
        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        protected void onPostExecute(Void result) {
            mDuration.setText(Support.getDurationTimeWithSeconds(mActuallTime - mCreatedTime));
        }
    }

    public void runUpdateTime(TextView durtaion, String createdTime) {
        Log.i(TAG, "runUpdateTime");
        this.mDuration = durtaion;
        mCreatedTime = Support.parseISO8601toDate(createdTime);
        mTimer = new Timer();
        mTimer.schedule(new CountTimer(), 1000, 1000);
    }

    public void stopUpdateTime() {
        if (mTimer != null) {
            Log.i(TAG, "removeUpdateTime");
            mTimer.cancel();
            mTimer = null;
        }
    }
}
