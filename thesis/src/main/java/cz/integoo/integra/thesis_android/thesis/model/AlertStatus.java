package cz.integoo.integra.thesis_android.thesis.model;

/**
 * Created by horkavlna on 24/08/14.
 */

public enum AlertStatus {
    NEW("new"), ACCEPTED("accepted"), CLOSED("closed");

    private final String status;

    AlertStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public static AlertStatus fromKey(String status) {
        for(AlertStatus type : AlertStatus.values()) {
            if(type.getStatus() == status) {
                return type;
            }
        }
        return null;
    }
}
