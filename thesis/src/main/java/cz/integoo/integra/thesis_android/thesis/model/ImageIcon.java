package cz.integoo.integra.thesis_android.thesis.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by horkavlna on 07/04/14.
 */

@DatabaseTable
public class ImageIcon {
    public static final String COLUMN_ID_IMAG_ICON = "_id";
    public static final String COLUMN_ID_INTEGOO = "id";
    public static final String COLUMN_IMAGE = "img";
    public static final String COLUMN_USER = "user";

    @DatabaseField(id = true, columnName = COLUMN_ID_IMAG_ICON)
    private long idImageIcon;
    @SerializedName(COLUMN_ID_INTEGOO)
    @DatabaseField(columnName = COLUMN_ID_INTEGOO)
    private String id;
    @DatabaseField(columnName = COLUMN_IMAGE, dataType = DataType.BYTE_ARRAY)
    private byte[] img;
    @ForeignCollectionField(eager = false, orderColumnName = Event.COLUMN_CREATED)
    private Collection<Event> event;
    @DatabaseField(canBeNull = false, foreign = true, columnName = COLUMN_USER)
    private User user;

    public long getIdImageIcon() {
        return idImageIcon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public Collection<Event> getEvent() {
        return event;
    }

    public void setEvent(Collection<Event> event) {
        this.event = event;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ImageIcon{" +
                "idImageIcon=" + idImageIcon +
                ", id=" + id +
                ", img=" + Arrays.toString(img) +
                ", event=" + event +
                ", user=" + user +
                '}';
    }
}
