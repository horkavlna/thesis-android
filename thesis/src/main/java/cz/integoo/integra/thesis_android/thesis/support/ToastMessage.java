package cz.integoo.integra.thesis_android.thesis.support;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cz.integoo.integra.thesis_android.thesis.R;

/**
 * Created by horkavlna on 26/03/14.
 */
public class ToastMessage implements Animation.AnimationListener {

    private static final String TAG = ToastMessage.class.getName();

    private View mViewToast;
    private Activity mActivity;
    private Animation mAnimationUp;
    private Animation mAnimationDown;
    private RelativeLayout mItem;
    private Handler mHandler;

    public ToastMessage(Activity activity, String message, Handler handler, int id) {
        mAnimationDown = AnimationUtils.loadAnimation(activity, R.anim.slide_down);
        mAnimationDown.setAnimationListener(this);

        mItem = (RelativeLayout)activity.findViewById(id);
		mViewToast = activity.getLayoutInflater().inflate(R.layout.toast, null);

        ((TextView) mViewToast.findViewById(R.id.toast_text)).setText(message);
        mViewToast.setVisibility(View.VISIBLE);
        mViewToast.startAnimation(mAnimationDown);
		mViewToast.setAlpha(255.0f);
        mItem.addView(mViewToast);
        this.mActivity = activity;
        this.mHandler = handler;
    }

	@Override
    public void onAnimationStart(Animation animation) {
        Log.i(ToastMessage.class.toString(),"onAnimationStart");
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Log.i(ToastMessage.class.toString(),"onAnimationEnd");
        if (mAnimationUp == null) {
            mAnimationUp = AnimationUtils.loadAnimation(mActivity, R.anim.slide_up);
            mAnimationUp.setStartOffset(2000);
            mAnimationUp.setAnimationListener(this);
            mViewToast.startAnimation(mAnimationUp);
        } else {
            mItem.removeView(mViewToast);
            mHandler.sendEmptyMessage(0);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        Log.i(ToastMessage.class.toString(),"onAnimationRepeat");
    }
}
