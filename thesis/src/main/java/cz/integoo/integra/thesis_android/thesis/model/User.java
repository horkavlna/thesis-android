package cz.integoo.integra.thesis_android.thesis.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;


/**
 * Created by horkavlna on 29/03/14.
 */

@DatabaseTable
public class User {

    public static final String COLUMN_ID_LOCATION = "_id";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_SELECTED_FILTER = "selectedfilter";

    @DatabaseField(columnName = COLUMN_ID_LOCATION, generatedId = true)
    private long id;
    @DatabaseField
    private String location;
    @DatabaseField
    private String imei;     //imei is not imei :D, but registrationGCM id
    @DatabaseField
    private String password;
    @DatabaseField(columnName = COLUMN_USERNAME)
    private String username;
    @ForeignCollectionField(eager = false, orderColumnName = Filter.COLUMN_NAME)
    private Collection<Filter> filter;
    @DatabaseField(canBeNull = true, foreign = true, columnName = COLUMN_SELECTED_FILTER)
    private Filter selectedFilter;
    @ForeignCollectionField(eager = false)
    private Collection<ImageIcon> icon;
    @ForeignCollectionField(eager = false, orderColumnName = Alert.COLUMN_CREATED_AT)
    private Collection<Alert> alert;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Collection<Alert> getAlert() {
        return alert;
    }

    public void setAlert(Collection<Alert> alert) {
        this.alert = alert;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Filter> getFilter() {
        return filter;
    }

    public void setFilter(Collection<Filter> filter) {
        this.filter = filter;
    }

    public Collection<ImageIcon> getIcon() {
        return icon;
    }

    public void setIcon(Collection<ImageIcon> icon) {
        this.icon = icon;
    }

    public Filter getSelectedFilter() {
        return selectedFilter;
    }

    public void setSelectedFilter(Filter selectedFilter) {
        this.selectedFilter = selectedFilter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        User c = (User) o;
        return c.getId() == getId();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", location='" + location + '\'' +
                ", imei='" + imei + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", filter=" + filter +
                ", selectedFilter=" + selectedFilter +
                ", icon=" + icon +
                ", alert=" + alert +
                '}';
    }
}
