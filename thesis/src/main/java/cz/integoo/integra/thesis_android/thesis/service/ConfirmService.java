package cz.integoo.integra.thesis_android.thesis.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.util.HashMap;

import cz.integoo.integra.thesis_android.thesis.R;
import cz.integoo.integra.thesis_android.thesis.support.Support;

/**
 * Created by horkavlna on 30/03/14.
 */
public class ConfirmService extends IntentService {

    private static final String TAG = ConfirmService.class.getName();

    private boolean success;

    public ConfirmService() {
        super("confirm service");
    }

    public ConfirmService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, " onHandleIntent ");
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            new ConfirmAsyncTask(bundle).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        }
    }

    @Override
    public boolean stopService(Intent name) {
        Log.d(TAG, " stopService ");
        return super.stopService(name);
    }

    //ASYNCTASK FOR CONNECTION
    private class ConfirmAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private Messenger mMessanger;
        private ImageHandler mHandler;
        private Bundle mBundle;
		private IntegooRestClient mIntegooRestClient;

		private ConfirmAsyncTask(Bundle bundle) {
            this.mBundle = bundle;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.i(TAG, " doInBackground url:" + mBundle.get(Support.EXTRA_URL));
            this.mMessanger = (Messenger) mBundle.get(Support.EXTRA_HANDLER);
            this.mHandler = new ImageHandler();
            return createHttpConnection((String) mBundle.get(Support.EXTRA_METHOD), (String) mBundle.get(Support.EXTRA_URL), (HashMap) mBundle.get(Support.EXTRA_HEADER),
                    (String) mBundle.get(Support.EXTRA_BODY), (String) mBundle.get(Support.EXTRA_USERNAME), (String) mBundle.get(Support.EXTRA_PASSWORD));
        }

        protected void onPostExecute(Boolean result) {
            Log.i(TAG, " onPostExecute");
            sendBackHeadler();
        }

        public void sendBackHeadler() {
            try {
                Message message = Message.obtain();
                Bundle bundle = new Bundle();
				if (success == false) {
					mIntegooRestClient.cancel(getApplicationContext());
					bundle.putString(Support.EXTRA_RESPONSE_ERROR, getApplicationContext().getString(R.string.alert_messsage_no_internet_connection));
					message.setData(bundle);
					mMessanger.send(message);
					return;
				} else if (mHandler.onError() == null) {
					bundle.putInt(Support.EXTRA_RESPONSE_STATUSCODE, mHandler.onStatusCode());
					bundle.putString(Support.EXTRA_RESPONSE_HEADER, mHandler.onHeader());
				}
                bundle.putString(Support.EXTRA_RESPONSE_ERROR, mHandler.onError());
                message.setData(bundle);
                mMessanger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public boolean createHttpConnection(String method, String url, HashMap<String, String> header, String body, String username, String password) {
			final int CHECK_MILISECONDS = 250;
			int numberIterate = 0;
			success = false;
			mIntegooRestClient = new IntegooRestClient(username, password);
            if (method.equals(Support.GET)) {
				mIntegooRestClient.get(getApplicationContext(), url, null, header, this.mHandler);
            }
            if (method.equals(Support.POST)) {
				mIntegooRestClient.post(getApplicationContext(), url, header, this.mHandler);
            }
			while (true) {
				if(success) {
					break;
				}
				numberIterate = CHECK_MILISECONDS + numberIterate;
				if (numberIterate >= Support.CONNECTION_TIMEOUT) {
					Log.i(ConfirmAsyncTask.class.getName(), " number iterate expire");
					break;
				}
				try {
					Thread.sleep(CHECK_MILISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
            return true;
        }
    }

    //HANDLER HTTP CONNECTION
    private class ImageHandler extends AsyncHttpResponseHandler implements ListenerHttpResponseHandler {

        private Integer mStatusCode;
        private Header[] mHeaders;
        private byte[] mResponseBody;
        private Throwable mError;

        private ImageHandler() {
        }

        @Override
        public void onStart() {
            super.onStart();
            Log.i(ImageHandler.class.getName(), " onStart ");
            // Initiated the request
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            super.onSuccess(statusCode, headers, responseBody);
            Log.i(TAG, " onSuccess - statusCode" + statusCode + " headers" +headers.toString());
            mStatusCode = statusCode;
            mHeaders = headers;
            mResponseBody = responseBody;
        }

		@Override
		public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
			super.onFailure(statusCode, headers, responseBody, error);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
			success = true;
		}

		@Override
		public void onFailure(int statusCode, Throwable error, String content) {
			super.onFailure(statusCode, error, content);
			Log.e(TAG, " onFailure: statusCode:" + statusCode + " error: "+ error);
			mStatusCode = statusCode;
			mError = error;
		}

        @Override
        public void onRetry() {
            super.onRetry();
            Log.d(TAG, " onRetry ");
            // Request was retried
        }

        @Override
        public void onProgress(int bytesWritten, int totalSize) {
            super.onProgress(bytesWritten, totalSize);
            Log.d(TAG, " onProgress ");
            // Progress notification
        }

        @Override
        public void onFinish() {
            super.onFinish();
            Log.d(TAG, " onFinish ");
			success = true;
			// Completed the request (either success or failure)
        }

        @Override
        public Integer onStatusCode() {
            Log.i(TAG, "status code:" + mStatusCode);
            return mStatusCode;
        }

        @Override
        public String onHeader() {
            String count = null;
            Header[] headers = mHeaders;
            for (Header header : headers) {
                Log.i(TAG, "header response key:"+ header.getName() +" value:" + header.getValue());
                if (header.getName().equals(Support.HEADER_X_COUNT)) {
                    count = header.getValue();
                }
            }
            return count;
        }

        @Override
        public byte[] onResponseBody() {
            Log.i(TAG, "body:"+ new String(mResponseBody));
            return mResponseBody;
        }

        @Override
        public String onError() {
            Log.i(TAG, "errorMessage:"+ mError);
            if (mError == null) {
                return null;
            }
            return mError.toString();
        }
    }
}
