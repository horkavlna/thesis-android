package cz.integoo.integra.thesis_android.thesis.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import cz.integoo.integra.thesis_android.thesis.service.BroadcastService;

/**
 * Created by horkavlna on 05/05/14.
 */

public class AlarmManagerReceiver extends BroadcastReceiver {
    private static final String TAG = AlarmManagerReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive");
		Intent intentBroadcastService = new Intent(context, BroadcastService.class);
        context.startService(intentBroadcastService);
    }
}
