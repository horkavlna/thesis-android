package cz.integoo.integra.thesis_android.thesis.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import java.util.ArrayList;

import cz.integoo.integra.thesis_android.thesis.fragment.EventFragment;
import cz.integoo.integra.thesis_android.thesis.fragment.ExistAlertFragment;
import cz.integoo.integra.thesis_android.thesis.fragment.NewAlertFragment;
import cz.integoo.integra.thesis_android.thesis.model.Alert;
import cz.integoo.integra.thesis_android.thesis.model.AlertStatus;
import cz.integoo.integra.thesis_android.thesis.model.Event;
import cz.integoo.integra.thesis_android.thesis.model.Filter;
import cz.integoo.integra.thesis_android.thesis.model.User;
import cz.integoo.integra.thesis_android.thesis.persistence.ApplicationDatabase;
import cz.integoo.integra.thesis_android.thesis.persistence.PersistentData;
import cz.integoo.integra.thesis_android.thesis.service.MenuService;
import cz.integoo.integra.thesis_android.thesis.support.Support;
import cz.integoo.integra.thesis_android.thesis.support.parceleable.UrlRequestParam;

/**
 * Created by horkavlna on 05/05/14.
 */
public class ConnectionReceiver extends BroadcastReceiver {
    private static final String TAG = ConnectionReceiver.class.getName();
    private PersistentData mPersistentData;
    private ApplicationDatabase mDatabase;
    private User mUser;
    private Filter mFilter;
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive");
        mContext = context;
        WakeLockHelper.lock(mContext);
        mPersistentData = new PersistentData(mContext);
        mDatabase = ApplicationDatabase.createManaged(mContext);

        if (Support.checkNetwork(context) && mPersistentData.getSignedUserName() != null) {
            mUser = mDatabase.getUser(mPersistentData.getSignedUserName());
            if (mUser != null) {
                callNewAlert();
            } else {
                releaseWakeLock();
            }
        } else {
            releaseWakeLock();
        }
    }

    public void callNewAlert() {
        Intent intentNewAlert = new Intent(mContext, MenuService.class);
        intentNewAlert.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerNewAlert));
        intentNewAlert.putExtra(Support.EXTRA_METHOD, Support.GET);
        intentNewAlert.putExtra(Support.EXTRA_URL, mPersistentData.getServerAddress() + Support.URL_ALERTS);
        intentNewAlert.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createAlarmRequestParam(), AlertStatus.NEW));
        intentNewAlert.putExtra(Support.EXTRA_HEADER, Support.createAllAlertsHttpHeader(mPersistentData.getFetchLimit().toString(), "0"));
        intentNewAlert.putExtra(Support.EXTRA_IS_ALERT, true);
        //TODO CREATE PARCELABLE AND SEND STRINGENTITY
        intentNewAlert.putExtra(Support.EXTRA_USERNAME, mUser.getUsername());
        intentNewAlert.putExtra(Support.EXTRA_PASSWORD, mUser.getPassword());
        intentNewAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startService(intentNewAlert);
    }

    private Handler handlerNewAlert = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handlerNewAlert");
            Bundle bundle = msg.getData();
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR,null);
            if (errorMessage == null) {
                Log.i(TAG, "save to database New Alert");
                ArrayList<Alert> myModelList = (ArrayList<Alert>) bundle.getSerializable(Support.EXTRA_RESPONSE_BODY);
                mDatabase.saveAlerts(myModelList, mUser, AlertStatus.NEW);
                NewAlertFragment newAlertFragment = NewAlertFragment.getInstanceBroadcast();
                if (newAlertFragment != null) {
                    newAlertFragment.reloadList();
                }
            }
            callExistAlert();
        }
    };

    public void callExistAlert() {
        Intent intentExistAlert = new Intent(mContext.getApplicationContext(), MenuService.class);
        intentExistAlert.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerExistAlert));
        intentExistAlert.putExtra(Support.EXTRA_METHOD, Support.GET);
        intentExistAlert.putExtra(Support.EXTRA_URL, mPersistentData.getServerAddress() + Support.URL_ALERTS);
        intentExistAlert.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createAlarmRequestParam(), AlertStatus.ACCEPTED));
        intentExistAlert.putExtra(Support.EXTRA_HEADER, Support.createAllAlertsHttpHeader(mPersistentData.getFetchLimit().toString(), "0"));
        intentExistAlert.putExtra(Support.EXTRA_IS_ALERT, true);
        //TODO CREATE PARCELABLE AND SEND STRINGENTITY
        intentExistAlert.putExtra(Support.EXTRA_USERNAME, mUser.getUsername());
        intentExistAlert.putExtra(Support.EXTRA_PASSWORD, mUser.getPassword());
        intentExistAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startService(intentExistAlert);
    }

    private Handler handlerExistAlert = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handlerExistAlert");
            Bundle bundle = msg.getData();
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR, null);
            if (errorMessage == null) {
                Log.i(TAG, "save to database Exist Alert");
                ArrayList<Alert> myModelList = (ArrayList<Alert>) bundle.getSerializable(Support.EXTRA_RESPONSE_BODY);
                mDatabase.saveAlerts(myModelList, mUser, AlertStatus.ACCEPTED);
                ExistAlertFragment existAlertFragment = ExistAlertFragment.getInstanceBroadcast();
                if (existAlertFragment != null) {
                    existAlertFragment.reloadList();
                }
            }
            callEvent();
        }
    };

    public void callEvent() {
        if(mUser.getSelectedFilter() != null) {
            mFilter = mDatabase.getFilterId(mUser.getSelectedFilter().getIdfilter());
            Intent intentEvent = new Intent(mContext, MenuService.class);
            intentEvent.putExtra(Support.EXTRA_HANDLER, new Messenger(handlerEvent));
            intentEvent.putExtra(Support.EXTRA_METHOD, Support.GET);
            intentEvent.putExtra(Support.EXTRA_URL, mPersistentData.getServerAddress() + Support.URL_EVENTS);
            intentEvent.putExtra(Support.EXTRA_REQUEST_PARAM, new UrlRequestParam(Support.createEventRequestParam()));
            intentEvent.putExtra(Support.EXTRA_HEADER,
                    Support.createAllEventHttpHeader(mPersistentData.getFetchLimit().toString(), "0", String.valueOf(mFilter.getId())));
            intentEvent.putExtra(Support.EXTRA_IS_ALERT, false);
            //TODO CREATE PARCELABLE AND SEND STRINGENTITY
            intentEvent.putExtra(Support.EXTRA_USERNAME, mUser.getUsername());
            intentEvent.putExtra(Support.EXTRA_PASSWORD, mUser.getPassword());
            intentEvent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startService(intentEvent);
        } else {
            releaseWakeLock();
        }
    }

    private Handler handlerEvent = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handlerEvent");
            Bundle bundle = msg.getData();
            String errorMessage = bundle.getString(Support.EXTRA_RESPONSE_ERROR, null);
            if (errorMessage == null) {
                Log.i(TAG, "save to database Event");
                ArrayList<Event> myModelList = (ArrayList<Event>) bundle.getSerializable(Support.EXTRA_RESPONSE_BODY);
                mDatabase.saveEvent(myModelList, mFilter);
                EventFragment eventFragment = EventFragment.getInstanceBroadcast();
                if (eventFragment != null) {
                    eventFragment.reloadList();
                }
            }
            releaseWakeLock();
        }
    };


    public void releaseWakeLock() {
        ApplicationDatabase.closeManaged();
        if (WakeLockHelper.isHeald()) {
            WakeLockHelper.release();
        }
    }
}
