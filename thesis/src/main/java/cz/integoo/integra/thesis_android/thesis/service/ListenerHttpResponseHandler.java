package cz.integoo.integra.thesis_android.thesis.service;

/**
 * Created by horkavlna on 28/03/14.
 */
public interface ListenerHttpResponseHandler {
    public Integer onStatusCode();
    public String onHeader();
    public byte[] onResponseBody();
    public String onError();

}
