# Thesis-Android #
This project was created as the my master thesis on [Faculty of Information Technologies, CTU in Prague](http://fit.cvut.cz/en). Thesis has been done on the request of Integoo company. Among the main reasons was the demand of customers for easier and faster manipulation with the basic elements of the system. The aims of the study are to gather information from Intedra system 3 and to provide them to the user of the mobile application.
A part of the study is also a REST API concept that will be consumated by mobile client. As an output, there will be a mobile application compa- tible with both Android and [iOS](https://bitbucket.org/horkavlna/thesis-ios) platforms. Consequently, this platform will be used by final consumers on a daily basis . Therefore, a sufficient system overview for customers of the respective user interface platform will be emphasized.

# License #
Thesis-Android by Filip Reznicek is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/). If you have information on the author about it ( email: reznifil[at]gmail[dot]com ).

![88x31.png](https://bitbucket.org/repo/9gAxdB/images/487679329-88x31.png)